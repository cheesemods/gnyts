﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test.Site
{
    public interface IProgressHandler
    {
        void ReportProgress(string text, float progress);
        void ClearProgress();
    }
}
