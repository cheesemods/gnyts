﻿using UnityEngine;

namespace Assets.MHYAssets
{
    [CreateAssetMenu(menuName = "ScriptableObjects/TerrainDataTunnel")]
    public class TerrainDataTunnel : ScriptableObject
    {
        public Texture2D m_MaskMap;
        public Texture2D m_BaseMap;
        public Texture2D m_SpecularMetallicMap;
        public Texture2D m_DetailTintColorMap;
        public Texture2D m_FadeTintColorMap;
        public Texture2D m_AlphaTexture0;
        public Texture2D m_AlphaTexture1;
        public int m_TintMask;
    }
}
