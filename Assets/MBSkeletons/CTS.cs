﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CTS
{
	public enum ShaderType
	{
		Unity,
		miHoYo
	}

	public enum TextureSize
	{
		Texture_64,
		Texture_128,
		Texture_256,
		Texture_512,
		Texture_1024,
		Texture_2048,
		Texture_4096,
        Texture_8192
    }

    // From modded Terrain
    public enum TessellationLevel
    {
        Disabled,
        DeformationOnly,
        Forced
    }

	[Serializable]
	public class CTSTerrainTextureDetails
	{
		public bool m_isOpenInEditor;

		public int m_textureIdx;

		public string m_name;

		public float m_detailPower;

		public Color m_tint;

		public float m_tintBrightness;

		public float m_smoothness;

		public int m_albedoIdx;

		public float m_albedoTilingClose;

		public float m_albedoTilingFar;

		public bool m_enableSnow;

		[NonSerialized]
		public bool m_albedoWasChanged;

		public Vector4 m_albedoAverage;

		[SerializeField]
		private Texture2D m_albedoTexture;

		public int m_normalIdx;

		public float m_normalStrength;

		[NonSerialized]
		public bool m_normalWasChanged;

		[SerializeField]
		private Texture2D m_normalTexture;

		[NonSerialized]
		public bool m_detailHeightWasChanged;

		[SerializeField]
		private Texture2D m_detailHeight;

		public int m_heightIdx;

		public float m_heightDepth;

		public float m_heightContrast;

		public float m_heightBlendClose;

		public float m_heightBlendFar;

		public float m_heightTesselationDepth;

		public float m_heightMin;

		public float m_heightMax;

		[NonSerialized]
		public bool m_heightWasChanged;

		[SerializeField]
		private Texture2D m_heightTexture;

		public int m_aoIdx;

		public float m_aoPower;

		[NonSerialized]
		public bool m_aoWasChanged;

		[SerializeField]
		private Texture2D m_aoTexture;

		public int m_emissionIdx;

		public float m_emissionStrength;

		[NonSerialized]
		public bool m_emissionWasChanged;

		[SerializeField]
		private Texture2D m_emissionTexture;

		public SceneSurfaceMaterial walkRunSurfaceType;

		public SceneSurfaceMaterial climbSurfaceType;

		public float m_RainNormalPowerScale;

		public float m_RainReflectIntensity;

		public bool m_useTint;

		public Texture2D m_baseTint;

		public Vector2 m_baseTintTiling;

		public Vector2 m_baseTintOffset;
	}

	[Serializable]
	public enum SceneSurfaceMaterial
	{
		Invalid,
		Grass,
		Dirt,
		Rock,
		Snow,
		Water,
		Tile,
		Sand,
		Wood,
		Iron,
		Tree,
		Ice,
		Marble,
		Carpet,
		WoodLog,
		Cloud,
		Coin,
		SnowRock
	}
}