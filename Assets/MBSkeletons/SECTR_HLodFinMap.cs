﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "ScriptableObjects/SECTR_HLodFinMap")]
public sealed class SECTR_HLodFinMap : ScriptableObject
{
	public List<SECTR_HLODFinInfo> lodFinInfos;
}