﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CTS
{
	[ExecuteInEditMode]
	[Serializable]
	public class CompleteTerrainShader : MonoBehaviour
	{
		/*[HideInInspector]*/
		public CTSResources m_resources;

		public string m_ctsDirectory;

		[SerializeField]
		private CTSProfile m_profileInstance;

		[SerializeField]
		public CTSProfile m_profileSource;

		[SerializeField]
		private string m_profilePath;

		[SerializeField]
		private Texture2D m_normalMap;

		[SerializeField]
		private bool m_bakeNormalMap;

		[SerializeField]
		private Texture2D m_colorMap;

		[SerializeField]
		private bool m_bakeColorMap;

		[SerializeField]
		private bool m_bakeGrassTextures;

		[SerializeField]
		private float m_bakeGrassMixStrength;

		[SerializeField]
		private float m_bakeGrassDarkenAmount;

		[SerializeField]
		private bool m_useCutout;

		[SerializeField]
		private bool m_useMaskmap;

		[SerializeField]
		private Texture2D m_cutoutMask;

		[SerializeField]
		private float m_cutoutHeight;

		[SerializeField]
		private Texture2D m_splat1;

		[SerializeField]
		private Texture2D m_splat2;

		[SerializeField]
		private Texture2D m_splat3;

		[SerializeField]
		private Texture2D m_splat4;

		[SerializeField]
		private Texture2D m_DetailTintColorTexture;

		[SerializeField]
		private Texture2D m_FadeTintColorTexture;

		[SerializeField]
		private ShaderType m_activeShaderType;

		[SerializeField]
		private Terrain m_terrain;

		[SerializeField]
		private Material m_material;

		// These aren't what the actual code looks like, but good enough
		public void ApplyShaderMaterial()
        {
			m_material = new Material(m_resources.shaders.miHoYoCTSShaderPCPS4);
			m_material.name = m_terrain.name + " " + m_terrain.GetInstanceID();
			m_material.hideFlags = HideFlags.HideInInspector;

			UpdateShader();

			m_terrain.materialType = Terrain.MaterialType.Custom;
			m_terrain.materialTemplate = m_material;
        }

		public void UpdateShader()
        {
			CTSProfile profile = m_profileSource;
			//m_material.SetTexture("_Perlin_Normal_Tex", profile.GlobalDeta);
        }
	}
}
