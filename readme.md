![logo](logo.png)

Testing grounds for Impact SECTR world loading.

## To use

Put either GAME_1_5, GAME_2_4, or GAME_2_5 in Edit -> Project Settings -> Player -> Other Settings -> Scripting Define Symbols.

You should also copy either cldb-1.5.dat, cldb-2.4.dat, or cldb-2.5.dat to cldb.dat.

The window should open automatically after a change and on startup. If you need to open it again, go into Window -> Open Test.Site.

## Some bugs

After loading a map, the terrain will be black. You can fix it by selecting one of the terrain chunks, clicking the settings icon under Terrain (Terrain Settings), double clicking the custom material, setting all Texture_X_TintEnabled to 1, setting Texture_X_Albedo_Index to X, and Tint Enhance Factor to around `2.4` (this is a hack since the terrain shader doesn't support normal engine lighting.) Later versions appear to be missing this field so YMMV.

## Credits

.blk reading code and structs by khang06

AssetTools.NET library and HKWorldEdit2 by nesrak1