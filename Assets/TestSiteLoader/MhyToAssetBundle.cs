﻿using AssetsTools.NET;
using AssetsTools.NET.Extra;
using LZ4ps;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Test.Site
{
    public static class MhyToAssetBundle
    {
        public static void Convert(ClassDatabaseFile cldbFile, Mhy0File inputMhy, Stream outputStream, string inputFileName)
        {
            using (MemoryStream blankBundleStream = new MemoryStream(BuildBlankBundle("2017.4.30f1", 0x06)))
            using (AssetsFileReader blankBundleReader = new AssetsFileReader(blankBundleStream))
            {
                AssetBundleFile blankBundleFile = new AssetBundleFile();
                blankBundleFile.Read(blankBundleReader);

                List<BundleReplacer> blankBundleFileReplacers = new List<BundleReplacer>();

                foreach (var entry in inputMhy.Entries)
                {
                    string fileName = entry.Key;
                    byte[] fileData = entry.Value;

                    // Probably stored somewhere in the mhy header, just don't know where.
                    bool hasSerializedData = !(fileName.EndsWith(".resS") || fileName.EndsWith(".resource") || fileName.EndsWith(".sharedAssets"));

                    if (fileName.Contains(".") && hasSerializedData)
                        throw new Exception("assert: !(fileName.Contains(\".\") && hasSerializedData) = " + fileName);

                    if (hasSerializedData)
                    {
                        bool success = true;
                        try
                        {
                            fileData = FixAssetsFile(cldbFile, fileData, inputFileName);
                        }
                        catch (Exception ex)
                        {
                            success = false;
                            Debug.LogError("FixAssetsFile failed. Details below.");
                            Debug.LogException(ex);

                            EditorUtility.ClearProgressBar();
                        }

                        if (!success)
                            throw new Exception("Stopping...");
                    }

                    blankBundleFileReplacers.Add(new BundleReplacerFromMemory(fileName, null, hasSerializedData, fileData, -1));
                }

                AssetsFileWriter blankBundleWriter = new AssetsFileWriter(outputStream);
                blankBundleFile.Write(blankBundleWriter, blankBundleFileReplacers);
            }
        }

        private static byte[] BuildBlankBundle(string fileEngineVersion, uint fileVersion)
        {
            MemoryStream bundleFileMs = new MemoryStream();
            AssetsFileWriter writer = new AssetsFileWriter(bundleFileMs);

            AssetBundleHeader06 bundleHeader6 = new AssetBundleHeader06()
            {
                signature = "UnityFS",
                fileVersion = fileVersion,
                minPlayerVersion = "5.x.x",
                fileEngineVersion = fileEngineVersion,
                totalFileSize = 0,
                compressedSize = 0,
                decompressedSize = 0,
                flags = 0x40
            };

            AssetBundleBlockAndDirectoryList06 bundleInf6 = new AssetBundleBlockAndDirectoryList06()
            {
                checksumLow = 0,
                checksumHigh = 0,
                blockCount = 0,
                blockInf = new AssetBundleBlockInfo06[0],
                directoryCount = 0,
                dirInf = new AssetBundleDirectoryInfo06[0]
            };

            /////////////////////

            bundleHeader6.Write(writer);

            if (bundleHeader6.fileVersion >= 7)
            {
                writer.Align16();
            }

            long bundleInfStart = writer.Position;
            bundleInf6.Write(writer);
            long bundleInfEnd = writer.Position;

            uint bundleInfSize = (uint)(bundleInfEnd - bundleInfStart);

            bundleHeader6.compressedSize = bundleInfSize;
            bundleHeader6.decompressedSize = bundleInfSize;
            bundleHeader6.totalFileSize = bundleInfEnd;

            writer.Position = 0;

            bundleHeader6.Write(writer);
            writer.Position = bundleInfEnd;

            byte[] blankBundleBytes = bundleFileMs.ToArray();

            return blankBundleBytes;
        }

        // Needs cleaning lol
        private static byte[] FixAssetsFile(ClassDatabaseFile cldbFile, byte[] mhyAssetsFile, string inputFileName)
        {
            using (MemoryStream readStream = new MemoryStream(mhyAssetsFile))
            using (AssetsFileReader reader = new AssetsFileReader(readStream))
            {
                MHYExtra.useMHYFormat = true;

                AssetsFile assetsFile = new AssetsFile(reader);
                AssetsFileTable assetsTable = new AssetsFileTable(assetsFile);

                ushort nextScriptIndex = 0;
                if (assetsTable.assetFileInfo.Any(si => si.scriptIndex != 0xffff))
                {
                    nextScriptIndex = (ushort)(assetsTable.assetFileInfo.Max(si => si.scriptIndex != 0xffff ? si.scriptIndex : 0) + 1);
                }

                long nextPathId = assetsTable.assetFileInfo.Max(si => si.index) + 1;

                List<AssetsReplacer> assetsReplacers = new List<AssetsReplacer>();
                List<Type_0D> newTypes = new List<Type_0D>();

                // Circumvents "A script behaviour (probably XXX?) has a different serialization layout when loading. (Read 44 bytes but expected XXX bytes)"
                List<ushort> monobsToTrim = new List<ushort>();

                using (MemoryStream writeStream = new MemoryStream())
                using (AssetsFileWriter writer = new AssetsFileWriter(writeStream))
                {
                    assetsFile.typeTree.hasTypeTree = true;
                    for (int i = 0; i < assetsFile.typeTree.unity5Types.Count; i++)
                    {
                        EditorUtility.DisplayProgressBar("Test.Site", "Converting type trees", (float)i / assetsFile.typeTree.unity5Types.Count);

                        Type_0D curType0d = assetsFile.typeTree.unity5Types[i];

                        // Convert custom MiHoYo types to MonoBehaviours
                        string customMhyTypeName;
                        if (MonoHashUtil.internalTypeIdToName.TryGetValue(curType0d.classId, out customMhyTypeName))
                        {
                            // Get MonoScript dependency and MonoBehaviour TypeTree info
                            HashGroup hashGroup = MonoHashUtil.GetHashGroupByName(customMhyTypeName);

                            // Add new MonoScript dependency
                            assetsFile.dependencies.dependencies.Add(new AssetsFileDependency()
                            {
                                bufferedPath = "",
                                assetPath = "",
                                originalAssetPath = "",
                                guid = new AssetsFileDependency.GUID128()
                                {
                                    mostSignificant = (long)ulong.Parse(hashGroup.monoGuid.Substring(0, 16), NumberStyles.HexNumber),
                                    leastSignificant = (long)ulong.Parse(hashGroup.monoGuid.Substring(16, 16), NumberStyles.HexNumber)
                                },
                                type = 0x3
                            });
                            assetsFile.dependencies.dependencyCount++;

                            // Make MonoBehaviour TypeTree entry
                            Type_0D newType0d = C2T5.Cldb2TypeTree(cldbFile, curType0d.classId);
                            newType0d.classId = 0x72;
                            newType0d.scriptIndex = nextScriptIndex;
                            newType0d.scriptHash1 = MonoHashUtil.GetUIntHash(hashGroup.monoHash, 0); //uint.Parse(hashGroup.monoHash.Substring(0, 8), NumberStyles.HexNumber);
                            newType0d.scriptHash2 = MonoHashUtil.GetUIntHash(hashGroup.monoHash, 1); //uint.Parse(hashGroup.monoHash.Substring(8, 8), NumberStyles.HexNumber);
                            newType0d.scriptHash3 = MonoHashUtil.GetUIntHash(hashGroup.monoHash, 2); //uint.Parse(hashGroup.monoHash.Substring(16, 8), NumberStyles.HexNumber);
                            newType0d.scriptHash4 = MonoHashUtil.GetUIntHash(hashGroup.monoHash, 3); //uint.Parse(hashGroup.monoHash.Substring(24, 8), NumberStyles.HexNumber);
                            newType0d.typeHash1 = MonoHashUtil.GetUIntHash(hashGroup.typeHash, 0); //uint.Parse(hashGroup.typeHash.Substring(0, 8), NumberStyles.HexNumber);
                            newType0d.typeHash2 = MonoHashUtil.GetUIntHash(hashGroup.typeHash, 1); //uint.Parse(hashGroup.typeHash.Substring(8, 8), NumberStyles.HexNumber);
                            newType0d.typeHash3 = MonoHashUtil.GetUIntHash(hashGroup.typeHash, 2); //uint.Parse(hashGroup.typeHash.Substring(16, 8), NumberStyles.HexNumber);
                            newType0d.typeHash4 = MonoHashUtil.GetUIntHash(hashGroup.typeHash, 3); //uint.Parse(hashGroup.typeHash.Substring(24, 8), NumberStyles.HexNumber);
                            newType0d.dependencies = new int[0];
                            newType0d.dependenciesCount = 0;
                            FixMHYTypeTree(cldbFile, newType0d, customMhyTypeName);
                            newTypes.Add(newType0d);

                            nextScriptIndex++;

                            // Add MonoBehaviour header to all assets that use this type
                            foreach (AssetFileInfoEx assetInfo in assetsTable.GetAssetsOfType(curType0d.classId))
                            {
                                reader.Position = assetInfo.absoluteFilePos;
                                byte[] originalAssetData = reader.ReadBytes((int)assetInfo.curFileSize);
                                byte[] fixedAssetData = FixMHYAsset(originalAssetData, customMhyTypeName, assetsFile.dependencies.dependencyCount);
                                assetsReplacers.Add(new AssetsReplacerFromMemory(0, assetInfo.index, newType0d.classId, newType0d.scriptIndex, fixedAssetData));
                            }
                        }
                        else if (curType0d.classId == 0x72)
                        {
                            string stringHash = MonoHashUtil.GetStringHash(curType0d.typeHash1, curType0d.typeHash2, curType0d.typeHash3, curType0d.typeHash4);
                            HashGroup hashGroup = MonoHashUtil.GetHashGroupByTypeHash(stringHash);

                            if (hashGroup == null)
                            {
                                Debug.Log("Failed to find script of hash " + stringHash + " in " + inputFileName + ", did you add it to MonoHashUtil?");
                                // This should probably be a function haha
                                // Generate TypeTree for regular asset
                                monobsToTrim.Add(curType0d.scriptIndex);
                                ClassDatabaseType cldbType = AssetHelper.FindAssetClassByID(cldbFile, (uint)curType0d.classId);
                                if (cldbType != null)
                                {
                                    Type_0D newType0d = C2T5.Cldb2TypeTree(cldbFile, cldbType);
                                    newType0d.scriptIndex = curType0d.scriptIndex;
                                    newType0d.scriptHash1 = curType0d.scriptHash1;
                                    newType0d.scriptHash2 = curType0d.scriptHash2;
                                    newType0d.scriptHash3 = curType0d.scriptHash3;
                                    newType0d.scriptHash4 = curType0d.scriptHash4;
                                    newType0d.typeHash1 = curType0d.typeHash1;
                                    newType0d.typeHash2 = curType0d.typeHash2;
                                    newType0d.typeHash3 = curType0d.typeHash3;
                                    newType0d.typeHash4 = curType0d.typeHash4;
                                    newType0d.dependencies = curType0d.dependencies;
                                    newTypes.Add(newType0d);
                                }
                            }
                            else
                            {
                                //string asmCsPath = Path.Combine("Library", Path.Combine("ScriptAssemblies", "Assembly-CSharp.dll"));
                                ClassDatabaseType cldbType = AssetHelper.FindAssetClassByID(cldbFile, (uint)curType0d.classId);
                                if (cldbType != null)
                                {
                                    Type_0D newType0d = C2T5.Cldb2TypeTree(cldbFile, cldbType);
                                    newType0d.scriptIndex = curType0d.scriptIndex;
                                    newType0d.scriptHash1 = curType0d.scriptHash1;
                                    newType0d.scriptHash2 = curType0d.scriptHash2;
                                    newType0d.scriptHash3 = curType0d.scriptHash3;
                                    newType0d.scriptHash4 = curType0d.scriptHash4;
                                    newType0d.typeHash1 = curType0d.typeHash1;
                                    newType0d.typeHash2 = curType0d.typeHash2;
                                    newType0d.typeHash3 = curType0d.typeHash3;
                                    newType0d.typeHash4 = curType0d.typeHash4;
                                    newType0d.scriptIndex = curType0d.scriptIndex;
                                    newType0d.dependencies = curType0d.dependencies;

                                    NetDeserializer deserializer = new NetDeserializer();
                                    deserializer.Read(hashGroup.type, assetsFile.header.format);

                                    TemplateFieldToType0D typeConverter = new TemplateFieldToType0D();
                                    typeConverter.TemplateToTypeField(deserializer.children, newType0d);

                                    TypeField_0D[] monoFields = typeConverter.TemplateToTypeField(deserializer.children, newType0d);

                                    newType0d.stringTable = typeConverter.stringTable;
                                    newType0d.stringTableLen = (uint)newType0d.stringTable.Length;
                                    newType0d.typeFieldsEx = newType0d.typeFieldsEx.Concat(monoFields).ToArray();
                                    newType0d.typeFieldsExCount = (uint)newType0d.typeFieldsEx.Length;

                                    newTypes.Add(newType0d);
                                }
                            }
                        }
                        else
                        {
                            // Generate TypeTree for regular asset
                            ClassDatabaseType cldbType = AssetHelper.FindAssetClassByID(cldbFile, (uint)curType0d.classId);
                            if (cldbType != null)
                            {
                                Type_0D newType0d = C2T5.Cldb2TypeTree(cldbFile, cldbType);
                                newType0d.scriptIndex = curType0d.scriptIndex;
                                newType0d.scriptHash1 = curType0d.scriptHash1;
                                newType0d.scriptHash2 = curType0d.scriptHash2;
                                newType0d.scriptHash3 = curType0d.scriptHash3;
                                newType0d.scriptHash4 = curType0d.scriptHash4;
                                newType0d.typeHash1 = curType0d.typeHash1;
                                newType0d.typeHash2 = curType0d.typeHash2;
                                newType0d.typeHash3 = curType0d.typeHash3;
                                newType0d.typeHash4 = curType0d.typeHash4;
                                newType0d.dependencies = curType0d.dependencies;
                                newTypes.Add(newType0d);
                            }
                        }
                    }

                    List<AssetFileInfoEx> shaderInfos = assetsTable.GetAssetsOfType((int)AssetClassID.Shader);
                    for (int i = 0; i < shaderInfos.Count; i++)
                    {
                        EditorUtility.DisplayProgressBar("Test.Site", "Fixing shaders", (float)i / shaderInfos.Count);

                        AssetFileInfoEx assetInfo = shaderInfos[i];
                        reader.Position = assetInfo.absoluteFilePos;
                        byte[] originalAssetData = reader.ReadBytes((int)assetInfo.curFileSize);
                        byte[] fixedAssetData = FixShader(cldbFile, originalAssetData);
                        assetsReplacers.Add(new AssetsReplacerFromMemory(0, assetInfo.index, (int)assetInfo.curFileType, 0xffff, fixedAssetData));
                    }

                    List<AssetFileInfoEx> monoScriptInfos = assetsTable.GetAssetsOfType((int)AssetClassID.MonoScript);
                    for (int i = 0; i < monoScriptInfos.Count; i++)
                    {
                        AssetFileInfoEx assetInfo = monoScriptInfos[i];

                        AssetTypeTemplateField monoTemp = new AssetTypeTemplateField();
                        monoTemp.FromClassDatabase(cldbFile, AssetHelper.FindAssetClassByID(cldbFile, (uint)AssetClassID.MonoScript), 0);
                        
                        AssetTypeValueField scriptBaseField = new AssetTypeInstance(monoTemp, reader, assetInfo.absoluteFilePos).GetBaseField();
                        scriptBaseField.Get("m_AssemblyName").GetValue().Set("Assembly-CSharp.dll");

                        assetsReplacers.Add(new AssetsReplacerFromMemory(0, assetInfo.index, (int)assetInfo.curFileType, 0xffff, scriptBaseField.WriteToByteArray()));
                    }

                    List<AssetFileInfoEx> monoBehaviourInfos = assetsTable.GetAssetsOfType((int)AssetClassID.MonoBehaviour);
                    for (int i = 0; i < monoBehaviourInfos.Count; i++)
                    {
                        AssetFileInfoEx assetInfo = monoBehaviourInfos[i];

                        if (!monobsToTrim.Contains(assetInfo.scriptIndex))
                            continue;

                        reader.Position = assetInfo.absoluteFilePos;
                        byte[] originalAssetData = reader.ReadBytes((int)assetInfo.curFileSize);
                        byte[] fixedAssetData = TrimMonoBehaviour(originalAssetData);
                        assetsReplacers.Add(new AssetsReplacerFromMemory(0, assetInfo.index, (int)assetInfo.curFileType, assetInfo.scriptIndex, fixedAssetData));
                    }

                    List<AssetFileInfoEx> terrainDataInfos = assetsTable.GetAssetsOfType((int)AssetClassID.TerrainData);
                    for (int i = 0; i < terrainDataInfos.Count; i++)
                    {
                        AssetFileInfoEx assetInfo = terrainDataInfos[i];

                        AssetTypeTemplateField terrainDataTemp = new AssetTypeTemplateField();
                        terrainDataTemp.FromClassDatabase(cldbFile, AssetHelper.FindAssetClassByID(cldbFile, (uint)AssetClassID.TerrainData), 0);
                        
                        AssetTypeValueField terrainDataBaseField = new AssetTypeInstance(terrainDataTemp, reader, assetInfo.absoluteFilePos).GetBaseField();

                        HashGroup hashGroup = MonoHashUtil.GetHashGroupByName("TerrainDataTunnel");

                        assetsFile.dependencies.dependencies.Add(new AssetsFileDependency()
                        {
                            bufferedPath = "",
                            assetPath = "",
                            originalAssetPath = "",
                            guid = new AssetsFileDependency.GUID128()
                            {
                                mostSignificant = (long)ulong.Parse(hashGroup.monoGuid.Substring(0, 16), NumberStyles.HexNumber),
                                leastSignificant = (long)ulong.Parse(hashGroup.monoGuid.Substring(16, 16), NumberStyles.HexNumber)
                            },
                            type = 0x3
                        });
                        assetsFile.dependencies.dependencyCount++;

                        AssetTypeValueField m_SplatDatabase = terrainDataBaseField.Get("m_SplatDatabase");
                        AssetTypeValueField m_AlphaTextures = m_SplatDatabase.Get("m_AlphaTextures").Get("Array");

                        int m_MaskMap_FileID  = m_SplatDatabase.Get("m_MaskMap").Get("m_FileID").GetValue().AsInt();
                        long m_MaskMap_PathID = m_SplatDatabase.Get("m_MaskMap").Get("m_PathID").GetValue().AsInt64();
                        int m_BaseMap_FileID  = m_SplatDatabase.Get("m_BaseMap").Get("m_FileID").GetValue().AsInt();
                        long m_BaseMap_PathID = m_SplatDatabase.Get("m_BaseMap").Get("m_PathID").GetValue().AsInt64();
                        int m_SpecularMetallicMap_FileID  = m_SplatDatabase.Get("m_SpecularMetallicMap").Get("m_FileID").GetValue().AsInt();
                        long m_SpecularMetallicMap_PathID = m_SplatDatabase.Get("m_SpecularMetallicMap").Get("m_PathID").GetValue().AsInt64();
                        int m_DetailTintColorMap_FileID  = m_SplatDatabase.Get("m_DetailTintColorMap").Get("m_FileID").GetValue().AsInt();
                        long m_DetailTintColorMap_PathID = m_SplatDatabase.Get("m_DetailTintColorMap").Get("m_PathID").GetValue().AsInt64();
                        int m_FadeTintColorMap_FileID  = m_SplatDatabase.Get("m_FadeTintColorMap").Get("m_FileID").GetValue().AsInt();
                        long m_FadeTintColorMap_PathID = m_SplatDatabase.Get("m_FadeTintColorMap").Get("m_PathID").GetValue().AsInt64();
                        int m_AlphaTexture0_FileID  = m_AlphaTextures[0].Get("m_FileID").GetValue().AsInt();
                        long m_AlphaTexture0_PathID = m_AlphaTextures[0].Get("m_PathID").GetValue().AsInt64();
                        int m_AlphaTexture1_FileID  = m_AlphaTextures[1].Get("m_FileID").GetValue().AsInt();
                        long m_AlphaTexture1_PathID = m_AlphaTextures[1].Get("m_PathID").GetValue().AsInt64();

                        int m_TintMask = m_SplatDatabase.Get("m_TintMask").GetValue().AsInt();

                        ClassDatabaseType cldbType = AssetHelper.FindAssetClassByID(cldbFile, (uint)AssetClassID.MonoBehaviour);

                        Type_0D newType0d = C2T5.Cldb2TypeTree(cldbFile, cldbType.classId);
                        newType0d.classId = 0x72;
                        newType0d.scriptIndex = 0;
                        newType0d.scriptHash1 = MonoHashUtil.GetUIntHash(hashGroup.monoHash, 0);
                        newType0d.scriptHash2 = MonoHashUtil.GetUIntHash(hashGroup.monoHash, 1);
                        newType0d.scriptHash3 = MonoHashUtil.GetUIntHash(hashGroup.monoHash, 2);
                        newType0d.scriptHash4 = MonoHashUtil.GetUIntHash(hashGroup.monoHash, 3);
                        newType0d.typeHash1 = MonoHashUtil.GetUIntHash(hashGroup.typeHash, 0);
                        newType0d.typeHash2 = MonoHashUtil.GetUIntHash(hashGroup.typeHash, 1);
                        newType0d.typeHash3 = MonoHashUtil.GetUIntHash(hashGroup.typeHash, 2);
                        newType0d.typeHash4 = MonoHashUtil.GetUIntHash(hashGroup.typeHash, 3);
                        newType0d.dependencies = new int[0];
                        newType0d.dependenciesCount = 0;

                        NetDeserializer deserializer = new NetDeserializer();
                        deserializer.Read(hashGroup.type, assetsFile.header.format);

                        TemplateFieldToType0D typeConverter = new TemplateFieldToType0D();
                        typeConverter.TemplateToTypeField(deserializer.children, newType0d);

                        TypeField_0D[] monoFields = typeConverter.TemplateToTypeField(deserializer.children, newType0d);

                        newType0d.stringTable = typeConverter.stringTable;
                        newType0d.stringTableLen = (uint)newType0d.stringTable.Length;
                        newType0d.typeFieldsEx = newType0d.typeFieldsEx.Concat(monoFields).ToArray();
                        newType0d.typeFieldsExCount = (uint)newType0d.typeFieldsEx.Length;
                        newTypes.Add(newType0d);

                        AssetTypeTemplateField tunnelMonoTempField = new AssetTypeTemplateField();
                        tunnelMonoTempField.From0D(newType0d, 0);

                        AssetTypeValueField tunnelMonoBaseField = ValueBuilder.DefaultValueFieldFromTemplate(tunnelMonoTempField);

                        tunnelMonoBaseField.Get("m_GameObject").Get("m_FileID").GetValue().Set(0);
                        tunnelMonoBaseField.Get("m_GameObject").Get("m_PathID").GetValue().Set(0);
                        tunnelMonoBaseField.Get("m_Enabled").GetValue().Set((byte)1);
                        tunnelMonoBaseField.Get("m_Script").Get("m_FileID").GetValue().Set(assetsFile.dependencies.dependencyCount);
                        tunnelMonoBaseField.Get("m_Script").Get("m_PathID").GetValue().Set(11500000L);
                        tunnelMonoBaseField.Get("m_Name").GetValue().Set("TerrainDataTunnel");

                        tunnelMonoBaseField.Get("m_MaskMap").Get("m_FileID").GetValue().Set(m_MaskMap_FileID);
                        tunnelMonoBaseField.Get("m_MaskMap").Get("m_PathID").GetValue().Set(m_MaskMap_PathID);
                        tunnelMonoBaseField.Get("m_BaseMap").Get("m_FileID").GetValue().Set(m_BaseMap_FileID);
                        tunnelMonoBaseField.Get("m_BaseMap").Get("m_PathID").GetValue().Set(m_BaseMap_PathID);
                        tunnelMonoBaseField.Get("m_SpecularMetallicMap").Get("m_FileID").GetValue().Set(m_SpecularMetallicMap_FileID);
                        tunnelMonoBaseField.Get("m_SpecularMetallicMap").Get("m_PathID").GetValue().Set(m_SpecularMetallicMap_PathID);
                        tunnelMonoBaseField.Get("m_DetailTintColorMap").Get("m_FileID").GetValue().Set(m_DetailTintColorMap_FileID);
                        tunnelMonoBaseField.Get("m_DetailTintColorMap").Get("m_PathID").GetValue().Set(m_DetailTintColorMap_PathID);
                        tunnelMonoBaseField.Get("m_FadeTintColorMap").Get("m_FileID").GetValue().Set(m_FadeTintColorMap_FileID);
                        tunnelMonoBaseField.Get("m_FadeTintColorMap").Get("m_PathID").GetValue().Set(m_FadeTintColorMap_PathID);
                        tunnelMonoBaseField.Get("m_AlphaTexture0").Get("m_FileID").GetValue().Set(m_AlphaTexture0_FileID);
                        tunnelMonoBaseField.Get("m_AlphaTexture0").Get("m_PathID").GetValue().Set(m_AlphaTexture0_PathID);
                        tunnelMonoBaseField.Get("m_AlphaTexture1").Get("m_FileID").GetValue().Set(m_AlphaTexture1_FileID);
                        tunnelMonoBaseField.Get("m_AlphaTexture1").Get("m_PathID").GetValue().Set(m_AlphaTexture1_PathID);

                        assetsReplacers.Add(new AssetsReplacerFromMemory(0, nextPathId, (int)AssetClassID.MonoBehaviour, 0, tunnelMonoBaseField.WriteToByteArray()));
                        nextPathId++;

                        // Don't remove this
                        assetsFile.preloadTable.items.Add(new AssetPPtr(assetsFile.dependencies.dependencyCount, 11500000L));
                        assetsFile.preloadTable.len++;

                        // AssetBundle stuff
                        AssetFileInfoEx assetBundleInf = assetsTable.GetAssetsOfType((int)AssetClassID.AssetBundle)[0];

                        AssetTypeTemplateField assetBundleTemp = new AssetTypeTemplateField();
                        assetBundleTemp.FromClassDatabase(cldbFile, AssetHelper.FindAssetClassByID(cldbFile, (uint)AssetClassID.AssetBundle), 0);

                        AssetTypeValueField assetBundleBaseField = new AssetTypeInstance(assetBundleTemp, reader, assetBundleInf.absoluteFilePos).GetBaseField();
                        
                        //////

                        AssetTypeValueField m_PreloadTable = assetBundleBaseField.Get("m_PreloadTable").Get("Array");
                        AssetTypeValueField m_PreloadTableEntry = ValueBuilder.DefaultValueFieldFromArrayTemplate(m_PreloadTable);

                        int preloadIndex = m_PreloadTable.GetValue().AsArray().size;

                        m_PreloadTableEntry.Get("m_FileID").GetValue().Set(assetsFile.dependencies.dependencyCount);
                        m_PreloadTableEntry.Get("m_PathID").GetValue().Set(11500000L);

                        m_PreloadTable.SetChildrenList(m_PreloadTable.children.Concat(new AssetTypeValueField[] { m_PreloadTableEntry }).ToArray());

                        //////

                        AssetTypeValueField m_Container = assetBundleBaseField.Get("m_Container").Get("Array");
                        AssetTypeValueField m_ContainerEntry = ValueBuilder.DefaultValueFieldFromArrayTemplate(m_Container);

                        m_ContainerEntry.Get("first").GetValue().Set("terraindatatunnel");
                        AssetTypeValueField m_ContainerEntryInfo = m_ContainerEntry.Get("second");
                        m_ContainerEntryInfo.Get("preloadIndex").GetValue().Set(preloadIndex);
                        m_ContainerEntryInfo.Get("preloadSize").GetValue().Set(1);
                        m_ContainerEntryInfo.Get("asset").Get("m_FileID").GetValue().Set(0);
                        m_ContainerEntryInfo.Get("asset").Get("m_PathID").GetValue().Set(nextPathId - 1);

                        m_Container.SetChildrenList(m_Container.children.Concat(new AssetTypeValueField[] { m_ContainerEntry }).ToArray());

                        assetsReplacers.Add(new AssetsReplacerFromMemory(0, 1, (int)AssetClassID.AssetBundle, 0xffff, assetBundleBaseField.WriteToByteArray()));
                    }

                    assetsFile.typeTree.unity5Types = newTypes;

                    MHYExtra.useMHYFormat = false;
                    assetsFile.Write(writer, 0, assetsReplacers, 0, cldbFile);
                    MHYExtra.useMHYFormat = true;

                    return writeStream.ToArray();
                }
            }
        }

        private static void FixMHYTypeTree(ClassDatabaseFile cldbFile, Type_0D originalType, string typeName)
        {
            List<TypeField_0D> typeFields = originalType.typeFieldsEx.ToList();

            // Remove base field (we'll replace it with the MonoBehaviour type name later)
            typeFields.RemoveAt(0);

            switch (typeName)
            {
                // gameObject + enabled (MonoBehaviour)
                case "MiHoYoGrassBlock":
                case "MiHoYoGrassLand":
                case "MiHoYoLodLoader":
                case "MiHoYoLodMeshGroup":
                case "MiHoYoTextureStreamingPreloader":
                    typeFields.RemoveRange(0, 4); // m_GameObject(3), m_Enabled(1)
                    break;
                // gameObject (Component)
                case "MiHoYoWindParameterConfigurator":
                case "MiHoYoInstanceColor":
                case "MiHoYoGrassGlobalConfigurator":
                case "MiHoYoVegetationInteractor":
                    typeFields.RemoveRange(0, 3); // m_GameObject(3)
                    break;
                // name (NamedObject)
                case "MiHoYoGrassData":
                    typeFields.RemoveRange(0, 4); // m_Name(4)
                    break;
                // none (ScriptableObject)
                case "MiHoYoBinData":
                    break;
            }

            // Copy everything from MonoBehaviour
            Type_0D monoBehaviourType = C2T5.Cldb2TypeTree(cldbFile, (int)AssetClassID.MonoBehaviour);
            for (int i = 0; i < monoBehaviourType.typeFieldsExCount; i++)
            {
                TypeField_0D moddedTypeField = monoBehaviourType.typeFieldsEx[i];
                // Needs fix to stop repeating
                if (moddedTypeField.nameStringOffset < 0x80000000)
                {
                    string nameString = moddedTypeField.GetNameString(monoBehaviourType.stringTable);
                    moddedTypeField.nameStringOffset = (uint)originalType.stringTable.Length;
                    originalType.stringTable += nameString + '\0';
                }
                if (moddedTypeField.typeStringOffset < 0x80000000)
                {
                    string typeString = moddedTypeField.GetTypeString(monoBehaviourType.stringTable);
                    moddedTypeField.typeStringOffset = (uint)originalType.stringTable.Length;
                    originalType.stringTable += typeString + '\0';
                }
                typeFields.Insert(i, moddedTypeField);
            }

            originalType.stringTableLen = (uint)originalType.stringTable.Length;
            originalType.typeFieldsEx = typeFields.ToArray();
            originalType.typeFieldsExCount = (uint)typeFields.Count;
        }

        private static byte[] FixMHYAsset(byte[] originalAssetData, string typeName, int monoScriptFileId)
        {
            byte[] bodyAssetData = null;

            AssetPPtr gameObjectPPtr = new AssetPPtr(0, 0);
            bool enabled = true;
            string name = "";

            using (MemoryStream ms = new MemoryStream(originalAssetData))
            using (AssetsFileReader r = new AssetsFileReader(ms))
            {
                switch (typeName)
                {
                    // gameObject + enabled (MonoBehaviour)
                    case "MiHoYoGrassBlock":
                    case "MiHoYoGrassLand":
                    case "MiHoYoLodLoader":
                    case "MiHoYoLodMeshGroup":
                    case "MiHoYoTextureStreamingPreloader":
                        gameObjectPPtr = new AssetsTools.NET.AssetPPtr(r.ReadInt32(), r.ReadInt64());
                        enabled = r.ReadByte() == 1;
                        r.Align();
                        break;
                    // gameObject (Component)
                    case "MiHoYoWindParameterConfigurator":
                    case "MiHoYoInstanceColor":
                    case "MiHoYoGrassGlobalConfigurator":
                    case "MiHoYoVegetationInteractor":
                        gameObjectPPtr = new AssetsTools.NET.AssetPPtr(r.ReadInt32(), r.ReadInt64());
                        break;
                    // name (NamedObject)
                    case "MiHoYoGrassData":
                        name = r.ReadCountStringInt32();
                        r.Align();
                        break;
                    // none (ScriptableObject)
                    case "MiHoYoBinData":
                        break;
                }
                bodyAssetData = r.ReadBytes((int)(r.BaseStream.Length - r.Position));
            }

            using (MemoryStream ms = new MemoryStream())
            using (AssetsFileWriter w = new AssetsFileWriter(ms))
            {
                w.bigEndian = false;

                // m_GameObject
                w.Write(gameObjectPPtr.fileID);
                w.Write(gameObjectPPtr.pathID);

                // m_Enabled
                w.Write(enabled);
                w.Align();

                // m_Script
                w.Write(monoScriptFileId);
                w.Write(11500000L);

                // m_Name
                w.WriteCountStringInt32(name);
                w.Align();

                // Rest of the data
                w.Write(bodyAssetData);

                return ms.ToArray();
            }
        }

        // Removes garbage right after decompressedLengths and right before compressedBlob
        private static byte[] FixShader(ClassDatabaseFile cldbFile, byte[] originalAssetData)
        {
            using (MemoryStream ms = new MemoryStream(originalAssetData))
            using (AssetsFileReader r = new AssetsFileReader(ms))
            {
                AssetTypeTemplateField shaderTemp = new AssetTypeTemplateField();
                shaderTemp.FromClassDatabase(cldbFile, AssetHelper.FindAssetClassByID(cldbFile, (uint)AssetClassID.Shader), 0);

                AssetTypeTemplateField compressedBlobTempField = shaderTemp.children.FirstOrDefault(f => f.name == "compressedBlob");
                if (compressedBlobTempField == null)
                    return null;
                compressedBlobTempField.children[0].valueType = EnumValueTypes.ByteArray;

                AssetTypeValueField shaderBaseField = new AssetTypeInstance(new[] { shaderTemp }, r, 0).GetBaseField();

                byte[] uncompressedBlob = shaderBaseField["compressedBlob"]["Array"].GetValue().AsByteArray().data;
                MemoryStream compressedBlobStream = new MemoryStream();

                AssetTypeValueField platforms = shaderBaseField["platforms"]["Array"];
                AssetTypeValueField offsets = shaderBaseField["offsets"]["Array"];
                AssetTypeValueField compressedLengths = shaderBaseField["compressedLengths"]["Array"];
                AssetTypeValueField decompressedLengths = shaderBaseField["decompressedLengths"]["Array"];

                List<uint> newOffsets = new List<uint>();
                List<uint> newCompressedLengths = new List<uint>();
                List<uint> newDecompressedLengths = new List<uint>();

                uint totalOffset = 0;

                int shaderPlatCount = platforms.value.AsArray().size;
                for (int i = 0; i < shaderPlatCount; i++)
                {
                    uint offset = offsets[i].value.AsUInt();
                    uint compressedLength = compressedLengths[i].value.AsUInt();
                    uint decompressedLength = decompressedLengths[i].value.AsUInt();

                    byte[] uncompressedBlock = new byte[compressedLength];
                    Buffer.BlockCopy(uncompressedBlob, (int)offset, uncompressedBlock, 0, (int)compressedLength);

                    byte[] compressedBlock = LZ4Codec.Encode32(uncompressedBlock, 0, uncompressedBlock.Length);
                    compressedLength = (uint)compressedBlock.Length;

                    shaderBaseField["compressedLengths"]["Array"][i].value.Set(compressedLength);
                    shaderBaseField["offsets"]["Array"][i].value.Set(totalOffset);

                    compressedBlobStream.Write(compressedBlock, 0, compressedBlock.Length);

                    totalOffset += compressedLength;
                }

                uint uncompressedBlobSize = (uint)uncompressedBlob.Length;

                shaderBaseField["compressedBlob"]["Array"].GetValue().Set(new AssetTypeByteArray
                {
                    data = compressedBlobStream.ToArray(),
                    size = (uint)compressedBlobStream.Length
                });

                return shaderBaseField.WriteToByteArray();
            }

            //long garbageIndex = -1; //scan for 0x04000000ffffffff, no idea why it's here
            //using (MemoryStream ms = new MemoryStream(originalAssetData))
            //using (BinaryReader r = new BinaryReader(ms))
            //{
            //    while (r.BaseStream.Position < r.BaseStream.Length)
            //    {
            //        if (r.ReadUInt32() == 4)
            //        {
            //            if (r.BaseStream.Position + 4 > r.BaseStream.Length)
            //                break;
            //
            //            if (r.ReadUInt32() == 0xffffffff)
            //            {
            //                garbageIndex = r.BaseStream.Position - 8;
            //                break;
            //            }
            //        }
            //    }
            //}
            //
            //if (garbageIndex == -1)
            //    throw new Exception("couldn't find garbage, this shader will break everything so good luck figuring this out.");
            //
            ////return originalAssetData;
            //
            //int restOfTheDataSize = (int)(originalAssetData.Length - (garbageIndex + 8));
            //Buffer.BlockCopy(originalAssetData, (int)(garbageIndex + 8), originalAssetData, (int)garbageIndex, restOfTheDataSize);
            //Array.Resize(ref originalAssetData, originalAssetData.Length - 8);
            //return originalAssetData;
        }

        private static byte[] TrimMonoBehaviour(byte[] originalAssetData)
        {
            byte[] assetData;

            using (MemoryStream msr = new MemoryStream(originalAssetData))
            using (AssetsFileReader r = new AssetsFileReader(msr))
            using (MemoryStream msw = new MemoryStream())
            using (AssetsFileWriter w = new AssetsFileWriter(msw))
            {
                r.bigEndian = false;
                w.bigEndian = false;

                w.Write(r.ReadInt32());
                w.Write(r.ReadInt64());
                w.Write(r.ReadByte()); r.Align(); w.Align();
                w.Write(r.ReadInt32());
                w.Write(r.ReadInt64());
                w.WriteCountStringInt32(r.ReadCountStringInt32()); r.Align(); w.Align();

                assetData = msw.ToArray();
            }

            return assetData;
        }
    }
}
