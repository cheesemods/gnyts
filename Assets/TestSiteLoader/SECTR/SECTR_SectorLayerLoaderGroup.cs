﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test.Site
{
    public class SECTR_SectorLayerLoaderGroup
    {
        public SECTR_Sector sector;
        public List<SECTR_LayerLoader> loaders = new List<SECTR_LayerLoader>();
    }
}
