﻿using AssetsTools.NET;
using AssetsTools.NET.Extra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Test.Site.Tools
{
    public class MonoHashGenerator : EditorWindow
    {
        private string selectedPath;

        [MenuItem("Window/Mono Hash Generator")]
        public static void ShowWindow()
        {
            MonoHashGenerator win = GetWindow<MonoHashGenerator>();
            win.selectedPath = null;
            //NetDeserializer.RecursiveTypeLoad();
        }

        private void GenerateHashes(string path)
        {
            MHYExtra.useMHYFormat = false;
            AssetsManager am = new AssetsManager();
            //am.LoadClassDatabase("cldb.dat");

            Dictionary<string, string> guidToName = new Dictionary<string, string>();
            AssetsFileInstance assetDatabaseInst = am.LoadAssetsFile(Path.Combine("Library", "assetDatabase3"), false);
            AssetFileInfoEx databaseInf = assetDatabaseInst.table.GetAssetsOfType((int)AssetClassID.AssetDatabaseV1)[0];
            AssetTypeValueField databaseBf = am.GetTypeInstance(assetDatabaseInst, databaseInf).GetBaseField();
            AssetTypeValueField databaseAssetsArray = databaseBf.Get("m_Assets").Get("Array");
            foreach (AssetTypeValueField child in databaseAssetsArray.children)
            {
                AssetTypeValueField guidField = child.Get("first");
                string guid = ""
                    + guidField.Get("data[1]").GetValue().AsUInt().ToString("x8")
                    + guidField.Get("data[0]").GetValue().AsUInt().ToString("x8")
                    + guidField.Get("data[3]").GetValue().AsUInt().ToString("x8")
                    + guidField.Get("data[2]").GetValue().AsUInt().ToString("x8");

                string name = child.Get("second").Get("mainRepresentation").Get("name").GetValue().AsString();

                guidToName[guid] = name;
            }

            //BundleFileInstance bunInst = am.LoadBundleFile(path);
            AssetsFileInstance assetsInst = am.LoadAssetsFile(path, false);

            using (StreamWriter sw = new StreamWriter(path + ".hashes"))
            {
                foreach (AssetFileInfoEx inf in assetsInst.table.GetAssetsOfType((int)AssetClassID.MonoBehaviour))
                {
                    Type_0D typeTreeType = assetsInst.file.typeTree.unity5Types[inf.curFileTypeOrIndex];
                    string typeHash = ""
                        + typeTreeType.typeHash1.ToString("x8")
                        + typeTreeType.typeHash2.ToString("x8")
                        + typeTreeType.typeHash3.ToString("x8")
                        + typeTreeType.typeHash4.ToString("x8");
                    string scriptHash = ""
                        + typeTreeType.scriptHash1.ToString("x8")
                        + typeTreeType.scriptHash2.ToString("x8")
                        + typeTreeType.scriptHash3.ToString("x8")
                        + typeTreeType.scriptHash4.ToString("x8");

                    AssetTypeValueField monoBehaviourBf = am.GetTypeInstance(assetsInst, inf).GetBaseField();
                    int depId = monoBehaviourBf.Get("m_Script").Get("m_FileID").GetValue().AsInt() - 1;
                    AssetsFileDependency scriptDep = assetsInst.file.dependencies.dependencies[depId];

                    string scriptDepHash = ""
                        + scriptDep.guid.mostSignificant.ToString("x8")
                        + scriptDep.guid.leastSignificant.ToString("x8");

                    string className;
                    if (!guidToName.TryGetValue(scriptDepHash, out className))
                    {
                        className = "unknown";
                    }

                    //AssetTypeValueField monoScriptBf = am.GetExtAsset(assetsInst, monoBehaviourBf.Get("m_Script")).instance.GetBaseField();
                    //AssetTypeValueField m_PropertiesHash = monoScriptBf.Get("m_PropertiesHash");
                    //
                    //string propertiesHash = "";
                    //for (int i = 0; i < 16; i++)
                    //{
                    //    propertiesHash += m_PropertiesHash.Get("bytes[" + i + "]").GetValue().AsInt().ToString("x2");
                    //}

                    //string m_ClassName = monoScriptBf.Get("m_ClassName").GetValue().AsString();

                    sw.Write("new HashGroup(\"" + className + "\", \"" + typeHash + "\", \"" + scriptHash + "\", \"" + scriptDepHash + "\"),\n");
                }
            }

            am.UnloadAll(true);
            MHYExtra.useMHYFormat = true;
        }

        private void OnGUI()
        {
            if (GUILayout.Button("Select Bundle"))
            {
                if (Selection.activeObject != null)
                {
                    selectedPath = AssetDatabase.GUIDToAssetPath(Selection.assetGUIDs[0]);
                    if (File.Exists(selectedPath))
                    {
                        using (FileStream fs = File.OpenRead(selectedPath))
                        using (AssetsFileReader r = new AssetsFileReader(fs))
                        {
                            // Basic validity
                            if (r.BaseStream.Length >= 0x30)
                            {
                                r.Position = 0x14;
                                if (r.ReadStringLength(11) != "2017.4.30f1")
                                {
                                    selectedPath = null;
                                }
                            }
                            else
                            {
                                selectedPath = null;
                            }
                        }
                    }
                }
                else
                {
                    selectedPath = null;
                }
            }

            if (selectedPath != null)
            {
                GUILayout.Label("Selected bundle: " + selectedPath);
            }

            if (GUILayout.Button("Generate Hashes"))
            {
                string selectedPathTmp = selectedPath;
                // In case of errors causing an infinite loop
                selectedPath = null;
                GenerateHashes(selectedPathTmp);
            }
        }
    }
}
