﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test.Site
{
    public class SECTR_AttachStreamLayer
    {
#if GAME_1_5
        public string _attachName;
#elif GAME_2_4 || GAME_2_5
        public uint _attachNameHash;
#endif
        public SECTR_CombineStreamPathInfo _festivalLayerPathInfo;
        public SECTR_CombineStreamPathInfo _routineLayerPathInfo;

        public SECTR_AttachStreamLayer(GenProtoReader r)
        {
            int memberExist = (int)r.ReadVarUInt32();
#if GAME_1_5
            if ((memberExist & 1) != 0)
            {
                _attachName = r.ReadVarString();
            }
            else
            {
                _attachName = string.Empty;
            }
#elif GAME_2_4 || GAME_2_5
            if ((memberExist & 1) != 0)
            {
                _attachNameHash = r.ReadVarUInt32();
            }
#endif
            if ((memberExist & 2) != 0)
            {
                _festivalLayerPathInfo = new SECTR_CombineStreamPathInfo(r);
            }
            if ((memberExist & 4) != 0)
            {
                _routineLayerPathInfo = new SECTR_CombineStreamPathInfo(r);
            }
        }
    }
    public class SECTR_CombineStreamPathInfo
    {
        public ulong _layerLoaderPathHash;
        public SECTR_PlatformStreamLayer[] _platformLayers;

        public SECTR_CombineStreamPathInfo(GenProtoReader r)
        {
            int memberExist = (int)r.ReadVarUInt32();
            if ((memberExist & 1) != 0)
            {
                _layerLoaderPathHash = r.ReadVarUInt64();
            }
            if ((memberExist & 2) != 0)
            {
                int __gny__platformLayersCount = (int)r.ReadVarUInt32();
                _platformLayers = new SECTR_PlatformStreamLayer[__gny__platformLayersCount];
                for (int i = 0; i < __gny__platformLayersCount; i++)
                {
                    _platformLayers[i] = new SECTR_PlatformStreamLayer(r);
                }
            }
        }
    }
    public class SECTR_LayerGroup
    {
#if GAME_1_5
        public string _groupName;
#elif GAME_2_4 || GAME_2_5
        public uint _groupNameHash;
#endif
        public SECTR_StreamLayer[] _layerInfos;

        public SECTR_LayerGroup(GenProtoReader r)
        {
            int memberExist = (int)r.ReadVarUInt32();
#if GAME_1_5
            if ((memberExist & 1) != 0)
            {
                _groupName = r.ReadVarString();
            }
            else
            {
                _groupName = string.Empty;
            }
#elif GAME_2_4 || GAME_2_5
            if ((memberExist & 1) != 0)
            {
                _groupNameHash = r.ReadVarUInt32();
            }
#endif
            if ((memberExist & 2) != 0)
            {
                int __gny__layerInfosCount = (int)r.ReadVarUInt32();
                _layerInfos = new SECTR_StreamLayer[__gny__layerInfosCount];
                for (int i = 0; i < __gny__layerInfosCount; i++)
                {
                    _layerInfos[i] = new SECTR_StreamLayer(r);
                }
            }
        }
    }
    public class SECTR_LayerLoader
    {
#if GAME_1_5
        public string _layerName;
#elif GAME_2_4 || GAME_2_5
        public uint _layerNameHash;
#endif
        public SECTR_StreamObject[] _virtualObjects;

        public SECTR_LayerLoader(GenProtoReader r)
        {
            int memberExist = (int)r.ReadVarUInt32();
#if GAME_1_5
            if ((memberExist & 1) != 0)
            {
                _layerName = r.ReadVarString();
            }
            else
            {
                _layerName = string.Empty;
            }
#elif GAME_2_4 || GAME_2_5
            if ((memberExist & 1) != 0)
            {
                _layerNameHash = r.ReadVarUInt32();
            }
#endif
            if ((memberExist & 2) != 0)
            {
                int __gny__virtualObjectsCount = (int)r.ReadVarUInt32();
                _virtualObjects = new SECTR_StreamObject[__gny__virtualObjectsCount];
                for (int i = 0; i < __gny__virtualObjectsCount; i++)
                {
                    _virtualObjects[i] = new SECTR_StreamObject(r);
                }
            }
        }
    }
    public class SECTR_PlatformStreamLayer
    {
        public SECTR_PlatformType _type;
        public ulong _layerLoaderPathHash;

        public SECTR_PlatformStreamLayer(GenProtoReader r)
        {
            int memberExist = (int)r.ReadVarUInt32();
            if ((memberExist & 1) != 0)
            {
                _type = (SECTR_PlatformType)r.ReadVarInt32();
            }
            if ((memberExist & 2) != 0)
            {
                _layerLoaderPathHash = r.ReadVarUInt64();
            }
        }
    }
    public class SECTR_Sector
    {
        public SECTR_LayerGroup[] _layerGroups;
#if GAME_1_5
        public string _sectorName;
#elif GAME_2_4 || GAME_2_5
        public uint _sectorNameHash;
#endif
        public SECTR_SectorType _type;
        public int widthIndexRawNum;
        public int heightIndexRawNum;
        public float distanceToChangeRawNum;
        public Vector _centerPos;

        public SECTR_Sector(GenProtoReader r)
        {
            int memberExist = (int)r.ReadVarUInt32();
            if ((memberExist & 1) != 0)
            {
                int __gny__layerGroupsCount = (int)r.ReadVarUInt32();
                _layerGroups = new SECTR_LayerGroup[__gny__layerGroupsCount];
                for (int i = 0; i < __gny__layerGroupsCount; i++)
                {
                    _layerGroups[i] = new SECTR_LayerGroup(r);
                }
            }
#if GAME_1_5
            if ((memberExist & 2) != 0)
            {
                _sectorName = r.ReadVarString();
            }
            else
            {
                _sectorName = string.Empty;
            }
#elif GAME_2_4 || GAME_2_5
            if ((memberExist & 2) != 0)
            {
                _sectorNameHash = r.ReadVarUInt32();
            }
#endif
            if ((memberExist & 4) != 0)
            {
                _type = (SECTR_SectorType)r.ReadVarInt32();
            }
            if ((memberExist & 8) != 0)
            {
                widthIndexRawNum = r.ReadVarInt32();
            }
            if ((memberExist & 16) != 0)
            {
                heightIndexRawNum = r.ReadVarInt32();
            }
            if ((memberExist & 32) != 0)
            {
                distanceToChangeRawNum = r.ReadSingle();
            }
            if ((memberExist & 64) != 0)
            {
                _centerPos = new Vector(r);
            }
            else
            {
                _centerPos = new Vector(0f, 0f, 0f);
            }    
        }
    }
    public class SECTR_StreamLayer
    {
#if GAME_1_5
        public string _sectorName;
        public string _layerName;
        public SECTR_LayerType _type;
        public string _layerFullName;
#elif GAME_2_4 || GAME_2_5
        public uint _sectorNameHash;
        public uint _layerNameHash;
        public SECTR_LayerType _type;
        public uint _layerFullNameHash;
#endif
        public Vector _position;
#if GAME_1_5
        public ulong _hlodPattern;
#endif
        public Vector _sectorSize;
        public SECTR_CombineStreamPathInfo _layerPathInfo;
        public SECTR_AttachStreamLayer[] _attachLayers;

        public SECTR_StreamLayer(GenProtoReader r)
        {
            int memberExist = (int)r.ReadVarUInt32();
#if GAME_1_5
            if ((memberExist & 1) != 0)
            {
                _sectorName = r.ReadVarString();
            }
            else
            {
                _sectorName = string.Empty;
            }
            if ((memberExist & 2) != 0)
            {
                _layerName = r.ReadVarString();
            }
            else
            {
                _layerName = string.Empty;
            }
            if ((memberExist & 4) != 0)
            {
                _type = (SECTR_LayerType)r.ReadVarInt32();
            }
            if ((memberExist & 8) != 0)
            {
                _layerFullName = r.ReadVarString();
            }
            else
            {
                _layerFullName = string.Empty;
            }
            if ((memberExist & 16) != 0)
            {
                _position = new Vector(r);
            }
            else
            {
                _position = new Vector(0f, 0f, 0f);
            }
            if ((memberExist & 32) != 0)
            {
                _hlodPattern = r.ReadVarUInt64();
            }
            if ((memberExist & 64) != 0)
            {
                _sectorSize = new Vector(r);
            }
            else
            {
                _sectorSize = new Vector(0f, 0f, 0f);
            }
            if ((memberExist & 128) != 0)
            {
                _layerPathInfo = new SECTR_CombineStreamPathInfo(r);
            }
            if ((memberExist & 256) != 0)
            {
                int __gny__attachLayersCount = (int)r.ReadVarUInt32();
                _attachLayers = new SECTR_AttachStreamLayer[__gny__attachLayersCount];
                for (int i = 0; i < __gny__attachLayersCount; i++)
                {
                    _attachLayers[i] = new SECTR_AttachStreamLayer(r);
                }
            }
#elif GAME_2_4 || GAME_2_5
            if ((memberExist & 1) != 0)
            {
                _sectorNameHash = r.ReadVarUInt32();
            }
            if ((memberExist & 2) != 0)
            {
                _layerNameHash = r.ReadVarUInt32();
            }
            if ((memberExist & 4) != 0)
            {
                _type = (SECTR_LayerType)r.ReadVarInt32();
            }
            if ((memberExist & 8) != 0)
            {
                _layerFullNameHash = r.ReadVarUInt32();
            }
            if ((memberExist & 16) != 0)
            {
                _position = new Vector(r);
            }
            else
            {
                _position = new Vector(0f, 0f, 0f);
            }
            if ((memberExist & 32) != 0)
            {
                _sectorSize = new Vector(r);
            }
            else
            {
                _sectorSize = new Vector(0f, 0f, 0f);
            }
            if ((memberExist & 64) != 0)
            {
                _layerPathInfo = new SECTR_CombineStreamPathInfo(r);
            }
            if ((memberExist & 128) != 0)
            {
                int __gny__attachLayersCount = (int)r.ReadVarUInt32();
                _attachLayers = new SECTR_AttachStreamLayer[__gny__attachLayersCount];
                for (int i = 0; i < __gny__attachLayersCount; i++)
                {
                    _attachLayers[i] = new SECTR_AttachStreamLayer(r);
                }
            }
#endif
        }
    }
    public class SECTR_StreamObject
    {
        public SECTR_StreamObjectType _type;
        public ulong _gameObjectPathHash;
#if GAME_1_5
        public string _objPatternName;
#elif GAME_2_4 || GAME_2_5
        public uint _objPatternNameHash;
#endif
        public float magitudeRawNum;
        public Vector _position;
        public Vector _rotation;
        public Vector _scale;

        public SECTR_StreamObject(GenProtoReader r)
        {
            int memberExist = (int)r.ReadVarUInt32();
            if ((memberExist & 1) != 0)
            {
                _type = (SECTR_StreamObjectType)r.ReadVarInt32();
            }
            if ((memberExist & 2) != 0)
            {
                _gameObjectPathHash = r.ReadVarUInt64();
            }
#if GAME_1_5
            if ((memberExist & 4) != 0)
            {
                _objPatternName = r.ReadVarString();
            }
            else
            {
                _objPatternName = string.Empty;
            }
#elif GAME_2_4 || GAME_2_5
            if ((memberExist & 4) != 0)
            {
                _objPatternNameHash = r.ReadVarUInt32();
            }
#endif
            if ((memberExist & 8) != 0)
            {
                magitudeRawNum = r.ReadSingle();
            }
            if ((memberExist & 16) != 0)
            {
                _position = new Vector(r);
            }
            else
            {
                _position = new Vector(0f, 0f, 0f);
            }
            if ((memberExist & 32) != 0)
            {
                _rotation = new Vector(r);
            }
            else
            {
                _rotation = new Vector(0f, 0f, 0f);
            }
            if ((memberExist & 64) != 0)
            {
                _scale = new Vector(r);
            }
            else
            {
                _scale = new Vector(0f, 0f, 0f);
            }
        }
    }
    public class Vector
    {
        public float _x;
        public float _y;
        public float _z;

        public Vector(GenProtoReader r)
        {
            int memberExist = (int)r.ReadVarUInt32();
            if ((memberExist & 1) != 0)
            {
                _x = r.ReadSingle();
            }
            if ((memberExist & 2) != 0)
            {
                _y = r.ReadSingle();
            }
            if ((memberExist & 4) != 0)
            {
                _z = r.ReadSingle();
            }
        }

        public Vector(float x, float y, float z)
        {
            _x = x;
            _y = y;
            _z = z;
        }
    }

    public enum SECTR_LayerType
    {
        Persistence,
        Terrain,
        StaticModel,
        StoneModel,
        DynamicModel,
        Navmesh,
        Collider,
        VeryHighModel,
        TerrainGrass,
        TreeModel,
        WaterTile,
        FogTexture,
        SmallLight,
        FarLight,
        DynamicSmallLight,
        DynamicFarLight,
        ReflectionProb
    }

    public enum SECTR_PlatformType
    {
        // GAME_2_4 didn't get this one so just adding it to GAME_1_5.
#if GAME_1_5 || GAME_2_4
        Base,
        _abilityElementStrengthNameCache,
        Mobile
#elif GAME_2_5
        Base,
        PC,
        Mobile,
        PS4Base
#endif
    }

    public enum SECTR_SectorType
    {
        Normal,
        City,
        Navmesh
    }

    public enum SECTR_StreamObjectType
    {
#if GAME_1_5
        None,
        Terrain,
        _expStruct,
        VO,
        Other,
        TerrainGrass,
        WaterTile,
        Navmesh,
        Occlusion,
        Particle,
        FogTexture,
        OnConfirmLogoutEnd,
        LimitRegion,
        VOCollider,
        LODCollider
#elif GAME_2_4
        None,
        Terrain,
        LOD,
        VO,
        Other,
        TerrainGrass,
        WaterTile,
        Navmesh,
        Occlusion,
        Particle,
        FogTexture,
        HLOD,
        LimitRegion,
        VOCollider,
        LODCollider,
        ReflectionProbTile
#elif GAME_2_5
        None,
        Terrain,
        LOD,
        VO,
        Other,
        TerrainGrass,
        WaterTile,
        Navmesh,
        Occlusion,
        Particle,
        FogTexture,
        HLOD,
        LimitRegion,
        VOCollider,
        LODCollider,
        ReflectionProbTile,
        HLODBlock
#endif
    }
}
