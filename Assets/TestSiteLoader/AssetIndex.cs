﻿using AssetsTools.NET;
using System.Collections.Generic;

namespace Test.Site
{
    public class AssetIndex
    {
        public int dataSize;
        public List<AssetTypeNameExtensionPair> assetTypeNameMap;
        public List<AssetInfo> assetInfoMap; //map asset id -> file name hash
        public List<BundleInfo> bundleInfoMap; //contains bundle dependency map
        public List<int> preloadBlocks;
        public List<int> preloadShaderBlocks;
        public List<BlkChunkListing> blockInfo1; //contains the blk names (ids) in each block folder
        public List<BlkBlockInfo> blockInfo2; //contains BlockInfo (list of file ids inside blk)
        public List<int> blockSortList; //no idea what this is for

        //custom stuff
        public Dictionary<long, int> assetHashToAssetId;
        public Dictionary<long, int> assetHash4ToAssetId;
        public Dictionary<int, List<int>> bundleDependencies;

        public AssetIndex(AssetsFileReader r)
        {
            int assetTypeNameMapCount = r.ReadInt32();
            assetTypeNameMap = new List<AssetTypeNameExtensionPair>();
            for (int i = 0; i < assetTypeNameMapCount; i++)
            {
                assetTypeNameMap.Add(new AssetTypeNameExtensionPair(r));
            }

            assetHashToAssetId = new Dictionary<long, int>();
            assetHash4ToAssetId = new Dictionary<long, int>();

            int assetInfoMapCount = r.ReadInt32();
            assetInfoMap = new List<AssetInfo>();
            for (int i = 0; i < assetInfoMapCount; i++)
            {
                AssetInfo assetInfo = new AssetInfo(r);
                assetInfoMap.Add(assetInfo);
                assetHashToAssetId[assetInfo.assetPathHash] = assetInfo.assetId;
                assetHash4ToAssetId[assetInfo.assetPathHash >> 8] = assetInfo.assetId;
            }

            bundleDependencies = new Dictionary<int, List<int>>();

            int bundleInfoMapCount = r.ReadInt32();
            bundleInfoMap = new List<BundleInfo>();
            for (int i = 0; i < bundleInfoMapCount; i++)
            {
                BundleInfo bundleInfo = new BundleInfo(r);
                bundleInfoMap.Add(bundleInfo);
                bundleDependencies[bundleInfo.assetId] = bundleInfo.dependencyIds;
            }

            int preloadBlockCount = r.ReadInt32();
            preloadBlocks = new List<int>();
            for (int i = 0; i < preloadBlockCount; i++)
            {
                preloadBlocks.Add(r.ReadInt32());
            }

            int preloadShaderBlockCount = r.ReadInt32();
            preloadShaderBlocks = new List<int>();
            for (int i = 0; i < preloadShaderBlockCount; i++)
            {
                preloadShaderBlocks.Add(r.ReadInt32());
            }

            int blockInfo1Count = r.ReadInt32();
            blockInfo1 = new List<BlkChunkListing>();
            for (int i = 0; i < blockInfo1Count; i++)
            {
                blockInfo1.Add(new BlkChunkListing(r));
            }

            int blockInfo2Count = r.ReadInt32();
            blockInfo2 = new List<BlkBlockInfo>();
            for (int i = 0; i < blockInfo2Count; i++)
            {
                blockInfo2.Add(new BlkBlockInfo(r));
            }

            int blockSortListCount = r.ReadInt32();
            blockSortList = new List<int>();
            for (int i = 0; i < blockSortListCount; i++)
            {
                blockSortList.Add(r.ReadInt32());
            }
        }

        public int GetAssetID(string path, bool hasExtension = false)
        {
            if (hasExtension)
            {
                long pathHash = (long)MhyHashUtil.MiHoYoGetAssetPathHash(path);
                if (assetHashToAssetId.ContainsKey(pathHash))
                {
                    return assetHashToAssetId[pathHash];
                }
            }
            else
            {
                // Some path hashes have extensions at the end which can be found in assetTypeNameMap
                // Brute force every extension to see if any hashes match
                foreach (AssetTypeNameExtensionPair pair in assetTypeNameMap)
                {
                    long pathHash = (long)MhyHashUtil.MiHoYoGetAssetPathHash(path + pair.extension);
                    if (assetHashToAssetId.ContainsKey(pathHash))
                    {
                        return assetHashToAssetId[pathHash];
                    }
                }
            }

            // Fallback if strings are available
            if (assetInfoMap.Count > 0 && assetInfoMap[0].assetPath != string.Empty)
            {
                for (int i = 0; i < assetInfoMap.Count; i++)
                {
                    if (assetInfoMap[i].assetPath == path)
                    {
                        return assetInfoMap[i].assetId;
                    }
                }
            }

            return -1;
        }
    }

    public class AssetTypeNameExtensionPair
    {
        public string typeName;
        public string extension;

        public AssetTypeNameExtensionPair(AssetsFileReader r)
        {
            typeName = r.ReadCountStringInt32();
            extension = r.ReadCountStringInt32();
        }
    }

    public class AssetInfo
    {
        public int assetId;
        public long assetPathHash; //first 7 bytes are the same as the "first" value in IndexObject entries
        public string assetPath;

        public AssetInfo(AssetsFileReader r)
        {
            assetId = r.ReadInt32();
            assetPathHash = r.ReadInt64();
            assetPath = r.ReadCountStringInt32();
        }
    }

    public class BundleInfo
    {
        public int assetId;
        public List<int> dependencyIds;

        public BundleInfo(AssetsFileReader r)
        {
            assetId = r.ReadInt32();
            int unk2Count = r.ReadInt32();
            dependencyIds = new List<int>();
            for (int i = 0; i < unk2Count; i++)
                dependencyIds.Add(r.ReadInt32());
        }
    }

    public class BlkChunkListing
    {
        public int chunkId;
        public List<int> entries;

        public BlkChunkListing(AssetsFileReader r)
        {
            chunkId = r.ReadInt32();
            int entriesCount = r.ReadInt32();
            entries = new List<int>();
            for (int i = 0; i < entriesCount; i++)
                entries.Add(r.ReadInt32());
        }
    }

    public class BlkBlockInfo
    {
        public int blkId;
        public List<BlkBlockInfoEntry> entries;

        public BlkBlockInfo(AssetsFileReader r)
        {
            blkId = r.ReadInt32();
            int entryCount = r.ReadInt32();
            entries = new List<BlkBlockInfoEntry>();
            for (int i = 0; i < entryCount; i++)
                entries.Add(new BlkBlockInfoEntry(r));
            entries.Sort((x, y) => x.offset.CompareTo(y.offset));
        }
    }

    public class BlkBlockInfoEntry
    {
        public int assetId;
        public int offset;

        public BlkBlockInfoEntry(AssetsFileReader r)
        {
            assetId = r.ReadInt32();
            offset = r.ReadInt32();
        }
    }
}
