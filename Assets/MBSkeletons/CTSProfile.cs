﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CTS
{
	[Serializable]
	[CreateAssetMenu(menuName = "ScriptableObjects/CTSProfile")]
	public class CTSProfile : ScriptableObject
	{
		[SerializeField]
		private int m_ctsMajorVersion;

		[SerializeField]
		private int m_ctsMinorVersion;

		public bool m_persistMaterials;

		public bool m_showGlobalSettings;

		public bool m_showSnowSettings;

		public bool m_showTextureSettings;

		public bool m_showGeoSettings;

		public bool m_showDetailSettings;

		public bool m_showColorMapSettings;

		public bool m_showOptimisationSettings;

		public string m_ctsDirectory;

		[SerializeField]
		public bool m_instanceChanged;

		[SerializeField]
		private ShaderType m_shaderType;

		public float m_globalUvMixPower;

		public float m_globalUvMixStartDistance;

		public float m_globalUvMixRange;

		public float m_globalNormalPower;

		public float m_globalDetailNormalClosePower;

		public float m_globalDetailNormalCloseTiling;

		public float m_globalDetailNormalFarPower;

		public float m_globalDetailNormalFarTiling;

		public float m_globalDetailHeightZScale;

		public float m_globalTerrainSmoothness;

		public float m_globalTerrainSpecular;

		public float m_globalBasemapDistance;

		public float m_globalSSAOIntensity;

		public float m_globalDeformationFakeAO;

		public bool m_globalOptimiseAtRuntime;

		public bool m_globalDisconnectAtRuntime;

		public TextureFormat m_albedoFormat;

		public int m_albedoAniso;

		public FilterMode m_albedoFilterMode;

		[SerializeField]
		private TextureSize m_albedoTextureSize;

		public int m_albedoTextureSizePx;

		[SerializeField]
		private bool m_albedoCompress;

		public TextureFormat m_normalFormat;

		public int m_normalAniso;

		public FilterMode m_normalFilterMode;

		[SerializeField]
		private TextureSize m_normalTextureSize;

		public int m_normalTextureSizePx;

		[SerializeField]
		private bool m_normalCompress;

		public bool enableFadeTint;

		public int m_globalDetailNormalMapIdx;

		[SerializeField]
		private Texture2D m_globalDetailNormalMap;

		[SerializeField]
		private bool m_enableSSS;

		[SerializeField]
		private /*Terrain.*/TessellationLevel m_tessellationLevel;

		[SerializeField]
		private List<CTSTerrainTextureDetails> m_terrainTextures;

		[SerializeField]
		private List<Texture2D> m_replacementTerrainAlbedos;

		[SerializeField]
		private List<Texture2D> m_replacementTerrainNormals;
	}
}
