﻿using UnityEngine;

namespace Assets.MHYAssets
{
    public class MiHoYoTextureStreamingPreloader : MonoBehaviour
	{
		public bool mPreloadFromHighToLow;
		public bool mWaitTextureStreaming;
	}
}
