﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using UnityEditor;

namespace Test.Site
{
    public static class MonoHashUtil
    {
        // Generate these with Window -> Mono Hash Generator
        // Note that if you make any changes to the structure of the classes,
        // you MUST delete and recreate the ScriptableObject/bundle/etc.
        // Unity will _not_ update the script hashes without doing so.
        public static HashGroup[] hashes = new HashGroup[]
        {
            // Fake custom assets
            new HashGroup("MiHoYoGrassBlock", "1f37aebf8ce21dfb841a47874ba62721", "1dff6259d37f2be4a634f99f435b07b1", "4dbe17d823b955fdb3a98083fa54fa83", typeof(Assets.MHYAssets.MiHoYoGrassBlock)),
            new HashGroup("MiHoYoGrassLand", "299563a6c3f221284b685a245866bd9e", "78cb8d88a5df0b22dae353fac624f8d7", "41fc87ab6dbae0521427c4127acef589", typeof(Assets.MHYAssets.MiHoYoGrassLand)),
            new HashGroup("MiHoYoWindParameterConfigurator", "609371bc9526f820083e9024be17e869", "f396adb03ad878106560721812baf640", "48117cbe5ad5566edba11b202060b68b", typeof(Assets.MHYAssets.MiHoYoWindParameterConfigurator)),
            new HashGroup("MiHoYoGrassData", "822e557a51e9a7d43fe4a63341b83cd3", "48161770a063abb3ec4af08d2a67518d", "4fe45bc763ac117f161c39775bd9438c", typeof(Assets.MHYAssets.MiHoYoGrassData)),
            new HashGroup("MiHoYoLodLoader", "5b97bf0f269f23b3b6f49d24c1cbb82d", "38c436ab5e30eae9fdddd51b47a3544e", "481538d1242c719f35c80fdbce3e55b2", typeof(Assets.MHYAssets.MiHoYoLodLoader)),
            new HashGroup("MiHoYoBinData", "fdcf28d04fe8cf848e2b2916d0619289", "8734e42b4a26b4f0136e45f99d9fe7a9", "4869a9f0f6a51b9d76db27b810c97798", typeof(Assets.MHYAssets.MiHoYoBinData)),
            new HashGroup("MiHoYoInstanceColor", "eaca30473ac2f96879da9f0a73447aed", "c3ff1827a4b6e8e88cddd61ae0e72d6d", "4c23764f825b3e466626b026e5d1a0b3", typeof(Assets.MHYAssets.MiHoYoInstanceColor)),
            new HashGroup("MiHoYoLodMeshGroup", "f2a26771e23133ec80f86e96a2c029e1", "0235c01cf3a576cdd087f44fe9de6704", "4862552a78816213cbb5a61cdee30db4", typeof(Assets.MHYAssets.MiHoYoLodMeshGroup)),
            new HashGroup("MiHoYoGrassGlobalConfigurator", "762d615e1188a9472f006e9d7776e667", "912f5911d24ba9228df7e6f673fec50a", "4a0500db230be4111a8265d27ace23a1", typeof(Assets.MHYAssets.MiHoYoGrassGlobalConfigurator)),
            new HashGroup("MiHoYoTextureStreamingPreloader", "8f3d7a33cb6ccfb4ef1e7e4600c7b92b", "2ad51e907940be2bb235ce923bf203e1", "4120807f9cce2484cb77f3c12dd66890", typeof(Assets.MHYAssets.MiHoYoTextureStreamingPreloader)),
            new HashGroup("MiHoYoVegetationInteractor", "b56a2568649180f07322e6275dac0ede", "4a9fc3451649f1686795dda680ecce3b", "4a3f61ba65453b2fbec405ea58821c80", typeof(Assets.MHYAssets.MiHoYoVegetationInteractor)),

            // Tunnels
            new HashGroup("TerrainDataTunnel", "8cb614dc580acc766495facbac0b616a", "2c300b92159a0afb2448f254709747b9", "411e1d184e70aec4a697ef9a7d122698", typeof(Assets.MHYAssets.TerrainDataTunnel)),

            // Actual MonoBehaviours
            new HashGroup("CTSProfile", "40bbeca438f9c5fa398d61b7fc1979a9", "188110fef4cb54722f2f48abe9656f26", "44b8effdb7554fd821ca194474bab6b6", typeof(CTS.CTSProfile)),
            new HashGroup("CTSResources", "de9fc06fa2d2df2a14a384fddcf11edc", "b142c00fffb71c297145abf90d4ba05f", "40adaf252cc2061e658657391704ea6", typeof(CTS.CTSResources)),
            new HashGroup("CompleteTerrainShader", "1796517826a20912bbaad073dd91bce3", "cbfcaa166a0e10a3730914994c845821", "4d8d433784c8a675b5ae43b9567a47ac", typeof(CTS.CompleteTerrainShader)),

#if GAME_1_5
            new HashGroup("SECTR_HLodFinMap", "cc8d98a4357d1e188fbc1029e3d376a9", "b4a9cd1a288160ec23026898576a077c", "4c72d82e420588d6e9f87ba50e183c80", typeof(SECTR_HLodFinMap)),
            new HashGroup("SECTR_LODConfig", "0836d2b821acf82e8c2f416c87496435", "932df9a7825c2d10ac5566b12006ff35", "44bc309bcc80adbccd95f8b4823ad7bd", typeof(SECTR_LODConfig)),
            new HashGroup("SECTR_LodFinMap", "46b99c37832bdc4801871fdc2486456b", "15d8c6d709026824ae48130b5b01d908", "4a49bf70526a657e19742e8dfd236e87", typeof(SECTR_LodFinMap)),
            new HashGroup("SECTR_VoFinMap", "d60b7030dd643af0e4fa46a166c2a70d", "2d00a354c3fe3e695dce178546928926", "418572d1af41a72d67c85dd80a3f2a0", typeof(SECTR_VoFinMap)),
#elif GAME_2_4 || GAME_2_5
            new HashGroup("SECTR_HLodFinMap", "53ca81fd9d4ce73a7a41d9727e402f8e", "b4a9cd1a288160ec23026898576a077c", "4c72d82e420588d6e9f87ba50e183c80", typeof(SECTR_HLodFinMap)),
            new HashGroup("SECTR_LODConfig", "4d4e2588ebb0d83a8c85f960031c49ee", "932df9a7825c2d10ac5566b12006ff35", "44bc309bcc80adbccd95f8b4823ad7bd", typeof(SECTR_LODConfig)),
            new HashGroup("SECTR_LodFinMap", "3e2b0498e7c7b78a22313edd210f471e", "15d8c6d709026824ae48130b5b01d908", "4a49bf70526a657e19742e8dfd236e87", typeof(SECTR_LodFinMap)),
            new HashGroup("SECTR_VoFinMap", "2b8f462fbf7e4a426aab0d1500c1d15a", "2d00a354c3fe3e695dce178546928926", "418572d1af41a72d67c85dd80a3f2a0", typeof(SECTR_VoFinMap)),
#endif
            // Tested 2.4 only
            new HashGroup("SectorBinData", "163ef0a9ffb9dfec0918da0eff4824cf", "2137f645b035a4f8a86143903b6c09c4", "403dff1ca4763b32a3846aa4a4592195", typeof(MoleMole.Config.SectorBinData))
        };

        public static Dictionary<int, string> internalTypeIdToName = new Dictionary<int, string>
        {
            { 0x25B, "MiHoYoGrassBlock" },
            { 0x25A, "MiHoYoGrassLand" },
            { 0x25D, "MiHoYoWindParameterConfigurator" },
            { 0x259, "MiHoYoGrassData" },
            { 0x4B4, "MiHoYoLodLoader" },
            { 0x4B8, "MiHoYoBinData" },
            { 0x4B7, "MiHoYoInstanceColor" },
            { 0x4B6, "MiHoYoLodMeshGroup" },
            { 0x25E, "MiHoYoGrassGlobalConfigurator" },
            { 0x4BB, "MiHoYoTextureStreamingPreloader" },
            { 0x25C, "MiHoYoVegetationInteractor" }
        };

        public static HashGroup GetHashGroupByName(string name)
        {
            return hashes.FirstOrDefault(h => h.name == name);
        }

        public static HashGroup GetHashGroupByTypeHash(string typeHash)
        {
            return hashes.FirstOrDefault(h => h.typeHash == typeHash);
        }

        public static string GetStringHash(uint hash1, uint hash2, uint hash3, uint hash4)
        {
            string hashString = ""
                + hash1.ToString("x8")
                + hash2.ToString("x8")
                + hash3.ToString("x8")
                + hash4.ToString("x8");

            return hashString;
        }

        public static uint GetUIntHash(string hashString, int index)
        {
            return uint.Parse(hashString.Substring(index * 8, 8), NumberStyles.HexNumber);
        }
    }

    public class HashGroup
    {
        public string name;
        public string typeHash;
        public string monoHash;
        public string monoGuid;
        // Type.GetType is unreliable
        public Type type;

        public HashGroup(string name, string typeHash, string monoHash, string monoGuid, Type type)
        {
            this.name = name;
            this.typeHash = typeHash;
            this.monoHash = monoHash;
            this.monoGuid = monoGuid;
            this.type = type;
        }
    }
}
