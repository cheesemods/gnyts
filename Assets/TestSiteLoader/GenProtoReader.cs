﻿using System.IO;
using System.Text;

namespace Test.Site
{
    public class GenProtoReader : BinaryReader
    {
        public GenProtoReader(Stream input) : base(input)
        {
        }

        public ulong ReadVarNumber(int limit = 10)
        {
            ulong result = 0;
            int shift = 0;
            byte b;
            do
            {
                b = ReadByte();
                result |= (uint)(b & 0x7f) << shift;
                limit--;
                shift += 7;
            } while (b > 0x7f && limit > 0);
            return result;
        }

        public byte ReadVarByte()
        {
            return (byte)ReadVarNumber();
        }
        
        public sbyte ReadVarSByte()
        {
            byte num = (byte)ReadVarNumber();
            return (sbyte)(num >> 1 ^ -(num & 1));
        }
        
        public ushort ReadVarUInt16()
        {
            return (ushort)ReadVarNumber();
        }
        
        public short ReadVarInt16()
        {
            ushort num = (ushort)ReadVarNumber();
            return (short)(num >> 1 ^ -(num & 1));
        }
        
        public uint ReadVarUInt32()
        {
            return (uint)ReadVarNumber();
        }
        
        public int ReadVarInt32()
        {
            uint num = (uint)ReadVarNumber();
            return (int)(num >> 1 ^ -(num & 1));
        }
        
        public ulong ReadVarUInt64()
        {
            return (ulong)ReadVarNumber();
        }
        
        public long ReadVarInt64()
        {
            ulong num = (ulong)ReadVarNumber();
            return (long)(num >> 1 ^ (ulong)-(long)(num & 1));
        }

        public string ReadVarString()
        {
            uint size = ReadVarUInt32();
            byte[] bytes = ReadBytes((int)size);
            return Encoding.UTF8.GetString(bytes);
        }
    }
}
