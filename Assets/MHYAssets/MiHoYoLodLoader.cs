﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.MHYAssets
{
    public class MiHoYoLodLoader : MonoBehaviour
    {
        public List<MiHoYoLodMeshGroup> m_Groups;
        public List<Transform> m_SortedBones;
        public List<uint> m_BoneHashes;
        public List<float> m_LodVisualSize;
        public List<Bounds> m_AABB;
        public float m_LodBufferZone;
        public float m_LodTransitionTime;
        public byte m_PauseAnimatorBelowLod;
    }
}
