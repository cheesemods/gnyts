﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "ScriptableObjects/SECTR_LODConfig")]
public sealed class SECTR_LODConfig : ScriptableObject
{
	public List<SECTR_LODPattern> lodPatterns;
}