﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CTS
{
	[CreateAssetMenu(menuName = "ScriptableObjects/CTSResources")]
	public sealed class CTSResources : ScriptableObject
	{
		public Shaders shaders;

		[Serializable]
		public sealed class Shaders
		{
			public Shader CTSShaderBasic;
			public Shader CTSShaderBasicCutout;
			public Shader CTSShaderAdvanced;
			public Shader CTSShaderAdvancedCutout;
			public Shader CTSShaderTesselated;
			public Shader CTSShaderTesselatedCutout;
			public Shader miHoYoCTSShader;
			public Shader miHoYoCTSShaderSecondPass;
			public Shader miHoYoCTSShaderPCPS4;
			public Shader miHoYoCTSShaderSecondPassPCPS4;
			public List<Material> miHoYoCTSInstancedMaterials;
		}
	}
}
