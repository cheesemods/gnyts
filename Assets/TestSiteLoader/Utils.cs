﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Test.Site
{
    public static class Utils
    {
        public static string FindDataDirectoryName(string directory)
        {
            foreach (string childDirectory in Directory.GetDirectories(directory))
            {
                if (childDirectory.EndsWith("_Data"))
                    return childDirectory;
            }
            return null;
        }
    }
}
