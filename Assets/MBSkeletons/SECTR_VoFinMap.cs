﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "ScriptableObjects/SECTR_VoFinMap")]
public sealed class SECTR_VoFinMap : ScriptableObject
{
	public List<SECTR_VOFinInfo> voFinInfos;
}