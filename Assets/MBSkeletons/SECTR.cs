﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SECTR_VOFinInfo
{
#if GAME_1_5
	public string name;
	public SECTR_ScenePropInfo scenePropInfo;
	public ulong pathHash;
	public ulong colliderPathHash;
	public ulong simpleColliderPathHash;
#elif GAME_2_4 || GAME_2_5
	public uint nameHash;
	public SECTR_ScenePropInfo scenePropInfo;
	public ulong pathHash;
	public ulong colliderPathHash;
	public ulong simpleColliderPathHash;
#endif
}

[Serializable]
public class SECTR_ScenePropInfo
{
#if GAME_1_5
	public ScenePropType type;
	public string scenePropPatternName;
	public string scenePropEffectPatternName;
#elif GAME_2_4 || GAME_2_5
	public ScenePropType type;
	public uint scenePropPatternNameHash;
	public uint scenePropEffectPatternNameHash;
#endif
}

public enum ScenePropType
{
	None,
	Tree,
	Bush,
	Stone,
	TreeLeaf,
	EnviroLight,
	RoomLight,
	Chair,
	EnviroCityLight,
	Count
}

[Serializable]
public class SECTR_LODFinInfo
{
#if GAME_1_5
	public string name;
	public string lodPatternName;
	public VirtualLODType objType;
	public SECTR_ScenePropInfo scenePropInfo;
	public List<SECTR_LODObjInfo> lodInfos;
	public float lodScale;
	public float cullScale;
	public ulong colliderPathHash;
	public ulong simpleColliderPathHash;
#elif GAME_2_4 || GAME_2_5
	public uint nameHash;
	public uint lodPatternNameHash;
	public VirtualLODType objType;
	public SECTR_ScenePropInfo scenePropInfo;
	public List<SECTR_LODObjInfo> lodInfos;
	public float lodScale;
	public float cullScale;
	public ulong colliderPathHash;
	public ulong simpleColliderPathHash;
#endif
}

public enum VirtualLODType
{
	Normal,
	Tree,
	Grass,
	Stone
}

[Serializable]
public class SECTR_LODObjInfo
{
	//doesn't need game variant
	public int lod;
	public ulong pathHash;
}

[Serializable]
public class SECTR_LODPattern
{
#if GAME_1_5
	public string patternName;
	public List<SECTR_LODQualityInfo> multiplatform;
	public List<SECTR_LODThreshold> lodThresholds;
#elif GAME_2_4 || GAME_2_5
	public uint patternNameHash;
	public List<SECTR_LODQualityInfo> multiplatform;
	public List<SECTR_LODThreshold> lodThresholds;
#endif
}

[Serializable]
public class SECTR_LODQualityInfo
{
#if GAME_1_5
	public string qualityName;
	public List<LODType> lodMask;
	public int beginGroupLOD;
	public bool enableFadeIn;
	public float platformViewScale;
	public float platformHeightScale;
#elif GAME_2_4
	public uint qualityNameHash;
	public List<LODType> lodMask;
	public int beginGroupLOD;
	public bool enableFadeIn;
	public float platformViewScale;
	public float platformHeightScale;
#elif GAME_2_5
	public uint qualityNameHash;
	public List<LODType> lodMask;
	public int beginGroupLOD;
	public bool enableFadeIn;
	public float platformViewScale;
	public float platformHeightScale;
	private int _lodHashSet;
#endif
}

public enum LODType
{
	LOD0,
	LOD1,
	LOD2,
	LOD3,
	LOD9 = 9,
	Count,
	Invalid = 999
}

[Serializable]
public class SECTR_LODThreshold
{
	//doesn't need game variant
	public LODType lod;
	public float screenPercent;
	public float distance;
}

[Serializable]
public class SECTR_HLODFinInfo
{
#if GAME_1_5
	public string name;
	public string lodPatternName;
	public List<SECTR_HLODObjInfo> hlodInfos;
	public float lodScale;
	public float cullScale;
#elif GAME_2_4 || GAME_2_5
	public uint nameHash;
	public uint lodPatternNameHash;
	public List<SECTR_HLODObjInfo> hlodInfos;
	public float lodScale;
	public float cullScale;
#endif
}

[Serializable]
public class SECTR_HLODObjInfo
{
#if GAME_1_5
	public int lod;
	public ulong pathHash;
	public List<SECTR_ActivityHLODObjInfo> activityInfo;
#elif GAME_2_4 || GAME_2_5
	public int lod;
	public ulong pathHash;
	public List<SECTR_ActivityHLODObjInfo> activityInfo;
#endif
}

[Serializable]
public class SECTR_ActivityHLODObjInfo
{
	//doesn't need game variant
	public string name;
	public ulong festivalPathHash;
	public ulong routinePathHash;
}