﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Test.Site
{
    public class Config : EditorWindow
    {
        public static void Open()
        {
            Config window = CreateInstance(typeof(Config)) as Config;
            window.titleContent = new GUIContent("Test.Site Config");
            window.minSize = new Vector2(230, 180);
            window.maxSize = new Vector2(230, 180);
            window.ShowUtility();
        }

        string gameDirectory = null;

        void OnGUI()
        {
            GUILayout.BeginVertical(GUILayout.Height(240));
            GUILayout.Label("First time setup");
            if (GUILayout.Button("Pick game directory"))
            {
                gameDirectory = EditorUtility.OpenFolderPanel("Choose game directory for Test.Site", null, null);
            }
            GUI.enabled = gameDirectory != null;
            if (GUILayout.Button("Continue"))
            {
                Close();
                File.WriteAllText("gamepath.txt", gameDirectory);
                Splash.Open();
            }
            GUI.enabled = true;
            GUILayout.EndVertical();
        }
    }
}