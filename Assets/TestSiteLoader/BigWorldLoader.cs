﻿using Assets.MHYAssets;
using AssetsTools.NET;
using AssetsTools.NET.Extra;
using CTS;
using MoleMole.Config;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Test.Site
{
    public class BigWorldLoader
    {
        public readonly BlkManager blkManager;
        public readonly AssetIndex assetIndex;
        public readonly AssetsManager assetsManager;
        public readonly Dictionary<long, string> hashLookup;
        public readonly Dictionary<int, AssetBundle> bundleLookup;
        public readonly Dictionary<TerrainData, TerrainDataTunnel> terrainLookup;

        public readonly SECTR_HLodFinMap hlodFinMap;
        public readonly SECTR_LODConfig lodConfig;
        public readonly SECTR_LodFinMap lodFinMap;
        public readonly SECTR_VoFinMap voFinMap;

        private readonly IProgressHandler _progressHandler;

        private readonly string _gameDir;
        private readonly string _gameDataDir;
        private readonly string _gameBlocksDir;

        private const string GAMEPATH_PATH = "gamepath.txt";
        private const string BLOCKS_PATH = "StreamingAssets/AssetBundles/blocks";
        private const string ASSET_INDEX_BLK_PATH = "00/31049740.blk";
        private const string HASH_LOOKUP_PATH = "hashlookup.txt";

        public BigWorldLoader(IProgressHandler progressHandler)
        {
            _progressHandler = progressHandler;

            if (!File.Exists(GAMEPATH_PATH))
                throw new FileNotFoundException("Game path not set");

            _gameDir = File.ReadAllText(GAMEPATH_PATH);
            _gameDataDir = Path.Combine(_gameDir, Utils.FindDataDirectoryName(_gameDir));
            _gameBlocksDir = Path.Combine(_gameDataDir, BLOCKS_PATH);

            assetIndex = LoadAssetIndex();
            blkManager = new BlkManager(assetIndex, _gameBlocksDir);
            assetsManager = new AssetsManager();
            assetsManager.LoadClassDatabase("cldb.dat");
            hashLookup = LoadHashLookup();
            bundleLookup = new Dictionary<int, AssetBundle>();
            terrainLookup = new Dictionary<TerrainData, TerrainDataTunnel>();

            //AssetBundle cacheBundle = AssetBundle.LoadFromFile("Cache/testlodconfig.unity3d");
            //foreach (var name in cacheBundle.GetAllAssetNames())
            //{
            //    Debug.Log("name: " + name);
            //}
            //
            //SECTR_LODConfig asset = cacheBundle.LoadAllAssets<SECTR_LODConfig>()[0];
            //Debug.Log(asset.lodPatterns.Count);
            //
            //return;

            hlodFinMap = ReadMonoBehaviourUnity<SECTR_HLodFinMap>("Build/LevelStreaming/Config/LODConfig/HLODFinInfoMap.ScriptableObject", true);
            lodConfig = ReadMonoBehaviourUnity<SECTR_LODConfig>("Build/LevelStreaming/Config/LODConfig/LODConfig.ScriptableObject", true);
            lodFinMap = ReadMonoBehaviourUnity<SECTR_LodFinMap>("Build/LevelStreaming/Config/LODConfig/LODFinInfoMap.ScriptableObject", true);
            voFinMap = ReadMonoBehaviourUnity<SECTR_VoFinMap>("Build/LevelStreaming/Config/LODConfig/VOFinInfoMap.ScriptableObject", true);
            //hlodFinMap = ReadMonoBehaviourNaive<SECTR_HLodFinMap>("Build/LevelStreaming/Config/LODConfig/HLODFinInfoMap.ScriptableObject");
            //lodConfig = ReadMonoBehaviourNaive<SECTR_LODConfig>("Build/LevelStreaming/Config/LODConfig/LODConfig.ScriptableObject");
            //lodFinMap = ReadMonoBehaviourNaive<SECTR_LodFinMap>("Build/LevelStreaming/Config/LODConfig/LODFinInfoMap.ScriptableObject");
            //voFinMap = ReadMonoBehaviourNaive<SECTR_VoFinMap>("Build/LevelStreaming/Config/LODConfig/VOFinInfoMap.ScriptableObject");

            Debug.Log("loaded " + hlodFinMap.lodFinInfos.Count + " hlodfininfos");
            Debug.Log("loaded " + lodConfig.lodPatterns.Count + " lodpatterns");
            Debug.Log("loaded " + lodFinMap.lodFinInfos.Count + " lodfininfos");
            Debug.Log("loaded " + voFinMap.voFinInfos.Count + " vofininfos");

            _progressHandler.ClearProgress();
        }

        private AssetIndex LoadAssetIndex()
        {
            _progressHandler.ReportProgress("Loading asset index blk...", 0.0f);
            string assetIndexBlkPath = Path.Combine(_gameBlocksDir, ASSET_INDEX_BLK_PATH);
            BlkFile assetIndexBlk = BlkManager.LoadBlkFile(assetIndexBlkPath);

            _progressHandler.ReportProgress("Loading asset index bundle...", 0.33f);
            Mhy0File assetIndexMhy = assetIndexBlk.ReadFile(0);
            byte[] assetIndexAssetsBytes = assetIndexMhy.Entries.First().Value;

            _progressHandler.ReportProgress("Loading asset index assets file...", 0.66f);
            using (MemoryStream assetIndexAssetsStream = new MemoryStream(assetIndexAssetsBytes))
            using (AssetsFileReader assetIndexAssetsReader = new AssetsFileReader(assetIndexAssetsStream))
            {
                MHYExtra.useMHYFormat = true;
                AssetsFile assetIndexAssetsFile = new AssetsFile(assetIndexAssetsReader);
                AssetsFileTable assetIndexAssetsTable = new AssetsFileTable(assetIndexAssetsFile);
                AssetFileInfoEx assetIndexInfo = assetIndexAssetsTable.GetAssetsOfType(0x4B8)[0];

                assetIndexAssetsReader.Position = assetIndexInfo.absoluteFilePos + 4;
                return new AssetIndex(assetIndexAssetsReader);
            }
        }

        private Dictionary<long, string> LoadHashLookup()
        {
            if (!File.Exists(HASH_LOOKUP_PATH))
                return null;

            Dictionary<long, string> lookup = new Dictionary<long, string>();
            using (StreamReader sr = new StreamReader(HASH_LOOKUP_PATH))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine().Trim();
                    if (line == string.Empty)
                        continue;

                    string[] lineSplit = line.Split(' ');
                    long hash = long.Parse(lineSplit[1], NumberStyles.HexNumber);
                    string path = lineSplit[2];
                    lookup[hash] = path;
                }
            }
            return lookup;
        }

        public AssetsFileInstance LoadAssetsFileFromBlk(string path, bool hasExtension = false)
        {
            int assetId = assetIndex.GetAssetID(path, hasExtension);
            if (assetId == -1)
                return null;

            Mhy0File mhyFile = blkManager.LoadMhyFile(assetId);
            foreach (var entry in mhyFile.Entries)
            {
                string fileName = entry.Key;
                byte[] fileData = entry.Value;

                // Probably stored somewhere in the mhy header, just don't know where.
                bool hasSerializedData = !(fileName.EndsWith(".resS") || fileName.EndsWith(".resource") || fileName.EndsWith(".sharedAssets"));

                if (fileName.Contains(".") && hasSerializedData)
                    throw new Exception("assert: !(fileName.Contains(\".\") && hasSerializedData) = " + fileName);

                if (hasSerializedData)
                {
                    MemoryStream assetsFileStream = new MemoryStream(fileData);

                    //File.WriteAllBytes("Dbg/" + path.Replace("/", "_").Replace(".", "_") + ".dat", fileData);

                    MHYExtra.useMHYFormat = true;
                    return assetsManager.LoadAssetsFile(assetsFileStream, "asset-" + path + ".mhy", false);
                }
            }

            return null;
        }

        public SECTR_SectorLayerLoaderGroup ReadBigWorldSectorLLG(string path)
        {
            string binDataPath = path + ".MiHoYoBinData";
            string indexDataPath = path + "_Index.ScriptableObject";

            AssetsFileInstance binDataFileInst = LoadAssetsFileFromBlk(binDataPath, true);
            if (binDataFileInst == null)
                return null;

            SectorBinData sectorBinData = ReadMonoBehaviourUnity<SectorBinData>(indexDataPath, true);

            AssetFileInfoEx info = binDataFileInst.table.GetAssetsOfType(0x4B8)[0];
            
            byte[] assetBytes = new byte[info.curFileSize - 4];
            binDataFileInst.stream.Position = info.absoluteFilePos + 4;
            binDataFileInst.stream.Read(assetBytes, 0, assetBytes.Length);

            //File.WriteAllBytes("Dbg/debug_debug_debug.dat", assetBytes);

            MemoryStream assetStream = new MemoryStream(assetBytes);
            return ReadBigWorldSectorStream(assetStream, sectorBinData);
        }

        private SECTR_SectorLayerLoaderGroup ReadBigWorldSectorStream(Stream blockStream, SectorBinData sectorBinData)
        {
            SECTR_SectorLayerLoaderGroup group = new SECTR_SectorLayerLoaderGroup();

            GenProtoReader reader = new GenProtoReader(blockStream);

            //long origPos = blockStream.Position;
            //byte[] debugDump = reader.ReadBytes((int)(blockStream.Length - blockStream.Position));
            //blockStream.Position = origPos;
            //File.WriteAllBytes("dbg_bwss.dat", debugDump);

            group.sector = new SECTR_Sector(reader);
            foreach (SectorBinData.Item data in sectorBinData.datas)
            {
                // happening in 2.5+
                if (data.offset >= reader.BaseStream.Length)
                    continue;

                reader.BaseStream.Position = data.offset;
                group.loaders.Add(new SECTR_LayerLoader(reader));
            }
            /*while (reader.BaseStream.Position < reader.BaseStream.Length)
            {
                group.loaders.Add(new SECTR_LayerLoader(reader));
            }*/
            return group;
        }

        public T ReadMonoBehaviourUnity<T>(string path, bool hasExtension = false) where T : UnityEngine.Object
        {
            AssetBundle unityBundle = GetCachedBundle(path, hasExtension);
            return unityBundle.LoadAllAssets<T>()[0];
        }

        //public T ReadMonoBehaviourNaive<T>(string path)
        //{
        //    AssetsFileInstance fileInst = LoadAssetsFileFromBlk(path, true);
        //    if (fileInst == null)
        //        return default(T);
        //
        //    AssetFileInfoEx info = fileInst.table.GetAssetsOfType(0x72)[0];
        //    fileInst.stream.Position = info.absoluteFilePos;
        //
        //    return (T)AssetConvertNaive.DeserializeObject(typeof(T), fileInst.file.reader);
        //}

        public AssetBundle GetCachedBundle(int assetId)
        {
            List<int> assetsToLoad = new List<int>();
            if (assetIndex.bundleDependencies.ContainsKey(assetId))
            {
                assetsToLoad = assetIndex.bundleDependencies[assetId];
            }
            assetsToLoad.Add(assetId);

            foreach (int assetToLoad in assetsToLoad)
            {
                string bundleCachePath = Path.Combine("Cache", "mhy-cache-" + assetToLoad + ".unity3d");
                if (File.Exists(bundleCachePath))
                    continue;

                Mhy0File mhyFile = blkManager.LoadMhyFile(assetToLoad);
                using (FileStream bundleStream = File.OpenWrite(bundleCachePath))
                {
                    MhyToAssetBundle.Convert(assetsManager.classFile, mhyFile, bundleStream, assetToLoad.ToString());
                }
            }

            foreach (int assetToLoad in assetsToLoad)
            {
                // GetAllLoadedAssetBundles is hard to use, use custom list
                if (bundleLookup.ContainsKey(assetToLoad))
                    continue;

                string bundleCachePath = Path.Combine("Cache", "mhy-cache-" + assetToLoad + ".unity3d");
                AssetBundle bundle = AssetBundle.LoadFromFile(bundleCachePath);

                // Should be expanded for more tunnel types in the future
                TerrainDataTunnel terrainDataTunnel = bundle.LoadAsset<TerrainDataTunnel>("terraindatatunnel");
                if (terrainDataTunnel != null)
                {
                    TerrainData terrainData = bundle.LoadAllAssets<TerrainData>()[0];
                    terrainLookup[terrainData] = terrainDataTunnel;
                }

                bundleLookup[assetToLoad] = bundle;
            }

            return bundleLookup[assetId];
        }

        public AssetBundle GetCachedBundle(string path, bool hasExtension = false)
        {
            int assetId = assetIndex.GetAssetID(path, hasExtension);
            if (assetId == -1)
                return null;

            //Debug.Log(path + " -> " + assetId);

            return GetCachedBundle(assetId);
        }

        public Texture2D GetMapTexture(int x, int y)
        {
            string mapPath = "ART/UI/Atlas/UIMap/TerrainCell/UI_MapBack_" + y + "_" + x;
            AssetBundle mapBundle = GetCachedBundle(mapPath, false);

            if (mapBundle == null)
                return null;

            string[] mapNames = mapBundle.GetAllAssetNames();

            if (mapNames.Length != 1)
            {
                Debug.Log("name count was not 1! " + mapNames.Length);
                return null;
            }
            else
            {
                string name = mapNames[0];
                //GameObject obj = mapBundle.LoadAsset<GameObject>(name);
                //return obj.GetComponent<SpriteRenderer>().sprite.texture;
                return mapBundle.LoadAsset<Texture2D>(name);
            }
        }

        public void SpawnLevel(string path)
        {
            AssetBundle levelBundle = GetCachedBundle(path, false);
            foreach (GameObject obj in levelBundle.LoadAllAssets<GameObject>())
            {
                UnityEngine.Object.Instantiate(obj);
            }

            EditorUtility.ClearProgressBar();
        }

        public void SpawnSector(int x, int y)
        {
            string llgPath = "Build/LevelStreaming/Level/OpenWorld/BigWorld/StreamGen/BigWorld_" + x + "_" + y;
            SECTR_SectorLayerLoaderGroup llg = ReadBigWorldSectorLLG(llgPath);

            //foreach (var loader in llg.loaders)
            for (int llgi = 0; llgi < llg.loaders.Count; llgi++)
            {
                EditorUtility.DisplayProgressBar("Test.Site", "Loading BigWorld", (float)llgi / llg.loaders.Count);

                var loader = llg.loaders[llgi];

                if (loader._virtualObjects == null)
                    continue;

                foreach (var vo in loader._virtualObjects)
                {
#if GAME_1_5
                    string objPatternName = vo._objPatternName;
#elif GAME_2_4 || GAME_2_5
                    uint objPatternNameHash = vo._objPatternNameHash;
#endif
                    Vector3 position = new Vector3(vo._position._x, vo._position._y, vo._position._z);
                    Quaternion rotation = Quaternion.Euler(new Vector3(vo._rotation._x, vo._rotation._y, vo._rotation._z));
                    Vector3 scale = new Vector3(vo._scale._x, vo._scale._y, vo._scale._z);

                    if (vo._type == SECTR_StreamObjectType.VO)
                    {
                        // needs to be dictionary
                        foreach (var voFinInfo in voFinMap.voFinInfos)
                        {
#if GAME_1_5
                            if (voFinInfo != null && voFinInfo.name != null && voFinInfo.name == objPatternName)
#elif GAME_2_4 || GAME_2_5
                            if (voFinInfo != null && voFinInfo.nameHash == objPatternNameHash)
#endif
                            {
                                int assetId;
                                if (!assetIndex.assetHashToAssetId.TryGetValue((long)voFinInfo.pathHash, out assetId))
                                {
                                    Debug.Log("couldn't find asset with hash " + voFinInfo.pathHash);
                                    break;
                                }

                                //int assetId = assetIndex.assetHashToAssetId[(long)voFinInfo.pathHash];
                                AssetBundle bundle = GetCachedBundle(assetId);
                                string[] names = bundle.GetAllAssetNames();

                                if (names.Length != 1)
                                {
                                    Debug.Log("name count was not 1! " + names.Length);
                                    break;
                                }

                                string name = names[0];

                                GameObject obj = bundle.LoadAsset<GameObject>(name);
                                //obj.name = objPatternName;
                                obj.transform.position = position;
                                obj.transform.localRotation = rotation;
                                obj.transform.localScale = scale;

                                UnityEngine.Object.Instantiate(obj);
                                break;
                            }
                        }
                    }
#if GAME_1_5
                    else if (vo._type == SECTR_StreamObjectType._expStruct)
#elif GAME_2_4 || GAME_2_5
                    else if (vo._type == SECTR_StreamObjectType.LOD)
#endif
                    {
                        // needs to be dictionary
                        foreach (var lodFinInfo in lodFinMap.lodFinInfos)
                        {
#if GAME_1_5
                            if (lodFinInfo != null && lodFinInfo.name != null && lodFinInfo.name == objPatternName)
#elif GAME_2_4 || GAME_2_5
                            if (lodFinInfo != null && lodFinInfo.nameHash == objPatternNameHash)
#endif
                            {
                                int assetId;
                                if (!assetIndex.assetHashToAssetId.TryGetValue((long)lodFinInfo.lodInfos[0].pathHash, out assetId))
                                {
                                    Debug.Log("couldn't find asset with hash " + lodFinInfo.lodInfos[0].pathHash);
                                    break;
                                }

                                AssetBundle bundle = GetCachedBundle(assetId);
                                string[] names = bundle.GetAllAssetNames();

                                if (names.Length != 1)
                                {
                                    Debug.Log("name count was not 1! " + names.Length);
                                    break;
                                }

                                string name = names[0];

                                GameObject obj = bundle.LoadAsset<GameObject>(name);
                                //obj.name = objPatternName;
                                obj.transform.position = position;
                                obj.transform.localRotation = rotation;
                                obj.transform.localScale = scale;

                                UnityEngine.Object.Instantiate(obj);
                                break;
                            }
                        }
                    }
#if GAME_2_4 || GAME_2_5
                    else if (vo._type == SECTR_StreamObjectType.HLOD)
                    {
                        // needs to be dictionary
                        foreach (var lodFinInfo in hlodFinMap.lodFinInfos)
                        {
                            if (lodFinInfo != null && lodFinInfo.nameHash == objPatternNameHash)
                            {
                                int assetId;
                                if (!assetIndex.assetHashToAssetId.TryGetValue((long)lodFinInfo.hlodInfos[0].pathHash, out assetId))
                                {
                                    Debug.Log("couldn't find asset with hash " + lodFinInfo.hlodInfos[0].pathHash);
                                    break;
                                }

                                AssetBundle bundle = GetCachedBundle(assetId);
                                string[] names = bundle.GetAllAssetNames();

                                if (names.Length != 1)
                                {
                                    Debug.Log("name count was not 1! " + names.Length);
                                    break;
                                }

                                string name = names[0];

                                GameObject obj = bundle.LoadAsset<GameObject>(name);
                                //obj.name = objPatternName;
                                obj.transform.position = position;
                                obj.transform.localRotation = rotation;
                                obj.transform.localScale = scale;

                                UnityEngine.Object.Instantiate(obj);
                                break;
                            }
                        }
                    }
#endif
                    else if (vo._type == SECTR_StreamObjectType.Terrain)
                    {
                        //fix to use correct path
                        string terrainPath = "Build/LevelStreaming/TerrainData_Final/OpenWorld/BigWorld/BigWorld_" + x + "_" + y + "/BigWorldTerrain_" + x + "_" + y + ".bin";
                        //int assetId = assetIndex.assetHash4ToAssetId[(long)vo._gameObjectPathHash];
                        AssetBundle terrainBundle = GetCachedBundle(terrainPath, false);
                        string[] terrainNames = terrainBundle.GetAllAssetNames();
                    
                        if (terrainNames.Length != 1)
                        {
                            Debug.Log("name count was not 1! " + terrainNames.Length);
                        }
                        else
                        {
                            string name = terrainNames[0];
                            GameObject obj = terrainBundle.LoadAsset<GameObject>(name);
                            //obj.name = objPatternName;
                            obj.transform.position = position;
                            obj.transform.localRotation = rotation;
                            obj.transform.localScale = scale;
                    
                            Terrain terrainComponent = obj.GetComponent<Terrain>();
                            TerrainData terrainData = terrainComponent.terrainData;
                            TerrainDataTunnel terrainTunnel = terrainLookup[terrainData];

                            //this should be moved to CompleteTerrainShader
                            CompleteTerrainShader cts = obj.GetComponent<CompleteTerrainShader>();
                            Shader ctsShader = cts.m_resources.shaders.miHoYoCTSShaderPCPS4;
                            Material ctsMaterial = new Material(ctsShader);
                            //ctsMaterial.SetTexture("_Texture_1_Albedo", terrainTunnel.m_BaseMap);
                            ctsMaterial.SetTexture("_Texture_Detail_Tint_Color", terrainTunnel.m_DetailTintColorMap);
                            ctsMaterial.SetTexture("_Texture_Fade_Tint_Color", terrainTunnel.m_FadeTintColorMap);
                            for (int i = 0; i < terrainData.splatPrototypes.Length; i++)
                            {
                                ctsMaterial.SetTexture("_Texture_" + (i + 1) + "_Albedo", terrainData.splatPrototypes[i].texture);
                            }
                            ctsMaterial.SetTexture("_Texture_Splat_1", terrainTunnel.m_AlphaTexture0);
                            ctsMaterial.SetTexture("_Texture_Splat_2", terrainTunnel.m_AlphaTexture1);

                            terrainComponent.materialType = Terrain.MaterialType.Custom;
                            terrainComponent.materialTemplate = ctsMaterial;


                            //Shader ctsShader = GetCachedBundle("", false);
                            //
                            //CompleteTerrainShader cts = new CompleteTerrainShader();

                            UnityEngine.Object.Instantiate(obj);
                        }
                    }
                    /*
                    else if (vo._type == SECTR_StreamObjectType.WaterTile)
                    {
                        string waterPath = "Build/LevelStreaming/WaterTile/OpenWorld/BigWorld/BigWorld_" + x + "_" + y + "/" + objPatternName;
                        //int assetId = assetIndex.assetHash4ToAssetId[(long)vo._gameObjectPathHash];
                        AssetBundle waterBundle = GetCachedBundle(waterPath, false);
                        string[] waterNames = waterBundle.GetAllAssetNames();

                        if (waterNames.Length != 1)
                        {
                            Debug.Log("name count was not 1! " + waterNames.Length);
                        }
                        else
                        {
                            string name = waterNames[0];
                            GameObject obj = waterBundle.LoadAsset<GameObject>(name);
                            //obj.name = objPatternName;
                            obj.transform.position = position;
                            obj.transform.localRotation = rotation;
                            obj.transform.localScale = scale;

                            UnityEngine.Object.Instantiate(obj);
                        }
                    }
                    */
            
                    ///GameObject dummyObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    ///dummyObj.name = objPatternName;
                    ///dummyObj.transform.position = position;
                    ///dummyObj.transform.localRotation = rotation;
                    ///dummyObj.transform.localScale = scale;
            
                    //UnityEngine.Object.Instantiate(dummyObj);
                }
            }

            EditorUtility.ClearProgressBar();
        }
    }
}