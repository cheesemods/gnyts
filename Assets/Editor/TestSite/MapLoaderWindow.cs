﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AssetsTools.NET;

namespace Test.Site
{
    public class MapLoaderWindow : EditorWindow
    {
        private BigWorldLoader _loader;
        
        private Dictionary<Vector2Int, Texture2D> mapImages;
        private RectInt mapBounds; // We use the bounds as x1,y1, x2,y2 instead of x,y, w,h.

        private Texture2D selectTexture;
        private GUIStyle coordStyle;

        private int viewX, viewY;

        private bool mouseDown;
        private bool mouseDragged;
        private int prevViewX, prevViewY;
        private Vector2 prevMousePosition;
        private Vector2 mousePosition;

        public static void Open()
        {
            MapLoaderWindow window = CreateInstance(typeof(MapLoaderWindow)) as MapLoaderWindow;
            window.titleContent = new GUIContent("Impact Test.Site BigWorld Loader (Map)");
            window.position = new Rect(window.position.x, window.position.y, 400f, 400f);
            window.ShowUtility();
        }

        void OnEnable()
        {
            _loader = new BigWorldLoader(new UnityProgressHandler());

            AssetBundle.UnloadAllAssetBundles(true);

            mapImages = new Dictionary<Vector2Int, Texture2D>();
            mapBounds = new RectInt(0, 0, 0, 0);

            selectTexture = new Texture2D(1, 1);
            selectTexture.SetPixel(0, 0, new Color(0.95f, 0f, 0f, 0.5f));
            selectTexture.wrapMode = TextureWrapMode.Repeat;
            selectTexture.Apply();

            coordStyle = new GUIStyle();
            coordStyle.normal.textColor = Color.white;
            coordStyle.fontSize = 20;

            for (int y = -15; y <= 15; y++)
            {
                for (int x = -15; x <= 15; x++)
                {
                    Texture2D tex = _loader.GetMapTexture(x, y);
                    if (tex == null)
                        continue;

                    if (x < mapBounds.x)
                        mapBounds.x = x;
                    else if (x > mapBounds.width)
                        mapBounds.width = x;
                    if (y < mapBounds.y)
                        mapBounds.y = y;
                    else if (y > mapBounds.height)
                        mapBounds.height = y;

                    // Negative hack lol
                    mapImages[new Vector2Int(-x, -y)] = tex;
                }
            }
        }

        void OnGUI()
        {
            wantsMouseMove = true;

            bool clicked = false;

            Event e = Event.current;
            if (e.isMouse)
            {
                if (e.type == EventType.MouseDown && e.button == 0)
                {
                    if (!mouseDown)
                    {
                        mouseDown = true;
                        mouseDragged = false;
                        prevMousePosition = e.mousePosition;
                        prevViewX = viewX;
                        prevViewY = viewY;
                    }
                }
                else if (e.type == EventType.MouseUp && e.button == 0)
                {
                    mouseDown = false;
                    if (prevMousePosition == e.mousePosition && !mouseDragged)
                    {
                        clicked = true;
                    }
                }
                
                if (e.type == EventType.MouseDrag && e.button == 0)
                {
                    if (mouseDown)
                    {
                        mouseDragged = true;
                        viewX = (int)(prevMousePosition.x - e.mousePosition.x + prevViewX);
                        viewY = (int)(prevMousePosition.y - e.mousePosition.y + prevViewY);
                    }
                }

                if (e.type == EventType.MouseMove || e.type == EventType.MouseDrag)
                {
                    mousePosition = e.mousePosition;
                    Repaint();
                }
            }

            const int MAP_TEXTURE_SIZE = 256;

            int windowWidth = (int)position.width;
            int windowHeight = (int)position.height;

            int xCount = windowWidth / MAP_TEXTURE_SIZE + 1;
            int yCount = windowHeight / MAP_TEXTURE_SIZE + 1;

            int xStart = DivDown(viewX, MAP_TEXTURE_SIZE);
            int yStart = DivDown(viewY, MAP_TEXTURE_SIZE);

            int xMove = Mod(viewX, MAP_TEXTURE_SIZE);
            int yMove = Mod(viewY, MAP_TEXTURE_SIZE);

            for (int yOff = 0; yOff < yCount + 1; yOff++)
            {
                for (int xOff = 0; xOff < xCount + 1; xOff++)
                {
                    Vector2Int coordinates = new Vector2Int(xStart + xOff, yStart + yOff);
                    if (mapImages.ContainsKey(coordinates))
                    {
                        Rect drawPosition = new Rect(
                            -xMove + xOff * MAP_TEXTURE_SIZE,
                            -yMove + yOff * MAP_TEXTURE_SIZE,
                            MAP_TEXTURE_SIZE, MAP_TEXTURE_SIZE
                        );
                        Texture2D mapImage = mapImages[coordinates];

                        if (drawPosition.Contains(mousePosition))
                        {
                            GUI.DrawTexture(drawPosition, mapImage, ScaleMode.StretchToFill, true, 0f, new Color(1f, 0f, 0f), 0f, 0f);
                            GUI.Label(drawPosition, (-(xStart + xOff)) + ", " + (-(yStart + yOff)), coordStyle);

                            if (clicked)
                            {
                                Close();
                                BigWorldLoaderWindow.Open(-(xStart + xOff), -(yStart + yOff));
                            }
                        }
                        else
                        {
                            GUI.DrawTexture(drawPosition, mapImage);
                        }
                    }
                }
            }
        }

        private int DivDown(int a, int b)
        {
            int res = a / b;
            return (a < 0 && a != b * res) ? res - 1 : res;
        }

        private int Mod(int x, int m)
        {
            int r = x % m;
            return r < 0 ? r + m : r;
        }
    }
}