﻿using System.Security.Cryptography;

namespace Test.Site
{
    public static class MhyHashUtil
    {
        public static ulong MiHoYoGetAssetPathHash(string path)
        {
            byte[] pathBytes = new byte[0x100];
            for (int i = 0; i < path.Length; i++)
            {
                pathBytes[i] = (byte)path[i];
            }

            MD5 md5 = MD5.Create();
            byte[] hashBytes = md5.ComputeHash(pathBytes);

            ulong result = 0;
            for (int i = 4; i >= 0; i--)
            {
                result = result * 0x100 + hashBytes[i];
            }
            return result;
        }
    }
}
