﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Test.Site
{
    [InitializeOnLoad]
    public static class SplashStarter
    {
        static SplashStarter()
        {
            EditorApplication.update += Startup;
        }
        static void Startup()
        {
            EditorApplication.update -= Startup;
            Splash.Open();
        }
    }

    public class Splash : EditorWindow
    {
        [MenuItem("Window/Open Test.Site")]
        public static void Open()
        {
            Splash window = CreateInstance(typeof(Splash)) as Splash;
            window.titleContent = new GUIContent("Test.Site");
            //window.minSize = new Vector2(490, 250);
            //window.maxSize = new Vector2(490, 250);
            window.ShowUtility();
        }

        string debugTextBox1 = "";
        string debugTextBox2 = "";

        void OnGUI()
        {
            Texture2D tex = new Texture2D(256, 256);
            ImageConversion.LoadImage(tex, File.ReadAllBytes("icon2.png"));
            GUILayout.BeginHorizontal();
            GUILayout.Label(tex);
            GUILayout.BeginVertical(GUILayout.Height(240));
            GUILayout.FlexibleSpace();
            GUILayout.Label("Test.Site Project\nExperimental SECTR Scene Loader");
            if (GUILayout.Button("Open BigWorld"))
            {
                Close();
                if (!File.Exists("gamepath.txt"))
                    Config.Open();
                else
                    BigWorldLoaderWindow.Open(-1, 0);
            }
            if (GUILayout.Button("Open normal level"))
            {
                Close();
            }
            if (GUILayout.Button("Open map view"))
            {
                Close();
                if (!File.Exists("gamepath.txt"))
                    Config.Open();
                else
                    MapLoaderWindow.Open();
            }
            //if (GUILayout.Button("(DEBUG) Run whole cache"))
            //{
            //    RunWholeCache();
            //    Close();
            //}
            if (GUILayout.Button("Close"))
            {
                Close();
            }
            //debugTextBox1 = GUILayout.TextField(debugTextBox1);
            //debugTextBox2 = GUILayout.TextField(debugTextBox2);
            GUILayout.EndHorizontal();
        }

        bool RunWholeCache()
        {
            int cacheId = 12345; //debugTextBox1
            string gameObjectName = "-12345678"; //debugTextBox2;
            //if (int.TryParse(debugTextBox1, out cacheId))
            {
                AssetBundle mainBundle = null;
                foreach (string fileName in Directory.GetFiles("Cache"))
                {
                    //for debug purposes
                    if (fileName.EndsWith("-"))
                        continue;

                    AssetBundle bundle = AssetBundle.LoadFromFile(fileName);
                    if (Path.GetFileName(fileName) == "mhy-cache-" + cacheId + ".unity3d")
                    {
                        mainBundle = bundle;
                    }
                }

                if (mainBundle == null)
                {
                    EditorUtility.DisplayDialog("Test.Site", "Couldn't find that Asset ID in the cache", "Ok");
                    return false;
                }

                GameObject mainGameObject = mainBundle.LoadAsset<GameObject>(gameObjectName);

                if (mainGameObject == null)
                {
                    EditorUtility.DisplayDialog("Test.Site", "Couldn't find that GameObject in the bundle", "Ok");
                    return false;
                }

                Instantiate(mainGameObject);

                return true;
            }
            //else
            {
                EditorUtility.DisplayDialog("Test.Site", "Asset ID not an integer", "Ok");
                return false;
            }
        }
    }
}