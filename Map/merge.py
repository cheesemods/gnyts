from PIL import Image
import os

RIGHT = -6
LEFT = 3
TOP = 4
BOTTOM = -7

IMG_SIZE = 1024

width = ((TOP+1)-(BOTTOM-1)) * IMG_SIZE
height = ((LEFT+1)-(RIGHT-1)) * IMG_SIZE
img = Image.new(mode = "RGB", size = (width, height))

for y in range(BOTTOM-2, TOP):
    for x in range(RIGHT-1, LEFT+1):
        imgX = (LEFT - x) * IMG_SIZE
        imgY = (TOP - y - 1) * IMG_SIZE
        
        print(f"map_{y}_{x}")
        
        partName = f"map_{y}_{x}.png"
        if not os.path.exists(partName):
            continue
        
        partImg = Image.open(partName)
        partImg = partImg.resize((IMG_SIZE, IMG_SIZE))
        img.paste(partImg, (imgX, imgY))

img.save("merge.png")