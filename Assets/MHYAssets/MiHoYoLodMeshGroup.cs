﻿using UnityEngine;

namespace Assets.MHYAssets
{
    public class MiHoYoLodMeshGroup : MonoBehaviour
	{
		public Mesh[] m_Meshes;
		public Mesh m_CurrentUsingMesh;
		public SkinnedMeshRenderer m_SkinnedMeshRenderer;
		public MeshRenderer m_MeshRenderer;
		public byte m_PreloadLod;
		public uint m_FadeMode;
		public uint m_FadeModeEx;
	}
}
