﻿using UnityEngine;

namespace Assets.MHYAssets
{
    public class MiHoYoWindParameterConfigurator : MonoBehaviour
	{
		public float[] m_LargeWindFrequency;
		public float[] m_LargeWindAmplitude;
		public float[] m_LargeWindScale;
		public float[] m_LargeWindDisplacementFrequency;
		public float[] m_LargeWindDisplacementAmplitude;
		public float[] m_LargeWindLevel;
		public Vector3 m_ForceWind;
		public bool m_UseForceWind;
	}
}
