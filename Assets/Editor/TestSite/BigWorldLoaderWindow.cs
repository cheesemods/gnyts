﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AssetsTools.NET;
using System;

namespace Test.Site
{
    public class BigWorldLoaderWindow : EditorWindow
    {
        private BigWorldLoader _loader;

        private int chunkX, chunkY;

        private string spawnText;

        public static void Open(int chunkX, int chunkY)
        {
            BigWorldLoaderWindow window = CreateInstance(typeof(BigWorldLoaderWindow)) as BigWorldLoaderWindow;
            window.titleContent = new GUIContent("Test.Site BigWorld Loader");
            window.chunkX = chunkX;
            window.chunkY = chunkY;
            window.ShowUtility();
            window.DoOpen();
        }

        void DoOpen()
        {
            _loader = new BigWorldLoader(new UnityProgressHandler());

            spawnText = "Path/To/Some/Asset";

            AssetBundle.UnloadAllAssetBundles(true);

            //_loader.SpawnLevel("Path/To/Some/Level/Name");
            _loader.SpawnSector(chunkX, chunkY);
        }

        [Obsolete("use _loader")]
        void LoadObject(int mainAssetId)
        {
            BundleInfo bundleInfo = _loader.assetIndex.bundleInfoMap.FirstOrDefault(inf => inf.assetId == mainAssetId);
            if (bundleInfo == null)
            {
                EditorUtility.DisplayDialog("Test.Site", "The object you opened is not in the listing but might be a dependency. Try selecting a different file.", "Ok");
                return;
            }

            List<int> assetIdsToLoad = new List<int>();
            assetIdsToLoad.Add(mainAssetId);
            assetIdsToLoad.AddRange(bundleInfo.dependencyIds);

            ClassDatabaseFile cldb = new ClassDatabaseFile();
            cldb.Read(new AssetsFileReader(File.OpenRead("cldb.dat")));

            foreach (int assetIdToLoad in assetIdsToLoad)
            {
                string bundleCacheName = GetBundleCacheName(assetIdToLoad);
                if (File.Exists(bundleCacheName))
                    continue;

                Mhy0File mhyFile = _loader.blkManager.LoadMhyFile(assetIdToLoad);
                using (FileStream bundleStream = File.OpenWrite(bundleCacheName))
                {
                    MhyToAssetBundle.Convert(cldb, mhyFile, bundleStream, assetIdToLoad.ToString());
                }
            }
        }

        void SpawnObject(string assetName)
        {
            int assetId = _loader.assetIndex.GetAssetID(assetName, false);
            if (assetId == -1)
            {
                Debug.Log("couldn't find asset with name " + assetName);
                return;
            }

            AssetBundle bundle = _loader.GetCachedBundle(assetId);
            string[] names = bundle.GetAllAssetNames();

            if (names.Length != 1)
            {
                Debug.Log("name count was not 1! " + names.Length);
                return;
            }

            string name = names[0];

            GameObject obj = bundle.LoadAsset<GameObject>(name);
            //obj.transform.position = SceneView.currentDrawingSceneView.pivot;
            Instantiate(obj);
        }

        string GetBundleCacheName(int assetId)
        {
            return Path.Combine("Cache", "mhy-cache-" + assetId + ".unity3d");
        }

        void OnGUI()
        {
            spawnText = GUI.TextField(new Rect(0, 0, position.width, 20), spawnText);
            if (GUI.Button(new Rect(0, 25, position.width, 20), "Spawn"))
            {
                SpawnObject(spawnText);
            }
        }
    }
}