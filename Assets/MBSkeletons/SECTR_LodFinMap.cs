﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "ScriptableObjects/SECTR_LodFinMap")]
public sealed class SECTR_LodFinMap : ScriptableObject
{
	public List<SECTR_LODFinInfo> lodFinInfos;
}