﻿using UnityEngine;

namespace Assets.MHYAssets
{
    public class MiHoYoGrassData : MonoBehaviour
	{
		public int m_OffsetX;
		public int m_OffsetY;
		public MiHoYoGrassLayer[] m_GrassLayers;
		public float[] m_SectorHeightMin;
		public float[] m_SectorHeightMax;
		public ushort[] m_SectorNormal;
		public byte[] m_SectorAreaColorR;
		public byte[] m_SectorAreaColorG;
		public byte[] m_SectorAreaColorB;
		public byte[] m_SectorRootColorR;
		public byte[] m_SectorRootColorG;
		public byte[] m_SectorRootColorB;
		public byte[] m_SectorAreaAttribute;
	}

	public class MiHoYoGrassLayer
    {
		public Material[] m_LodMaterials;
		public Mesh[] m_LodMeshs;
		public ushort m_GrassInstanceCount;
		public MiHoYoVegetationLayerData m_GrassBitmap;
		public int m_ActiveQualityLevel;
		public ulong m_GrassFlags;
		public Material m_FadingMat;
	}

	public class MiHoYoVegetationLayerData
    {
		public uint[] m_Bitmap;
		public int m_Width;
		public int m_Height;
		public uint[] m_HeightScales;
	}
}
