﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Extra
{
    [Serializable]
    [CreateAssetMenu(menuName = "ScriptableObjects/TestObj")]
    public class TestObj : ScriptableObject
    {
        public int a;
        public int b;
        public List<int> c;
        public List<string> d;
        public List<uint> e;
        public List<byte> f;
        public List<TestObj2> g;
        public int[] h;
        public string[] i;
        public uint[] j;
        public byte[] k;
        public TestObj2[] l;
    }

    [Serializable]
    public class TestObj2
    {
        public int a;
        public int b;
    }
}
