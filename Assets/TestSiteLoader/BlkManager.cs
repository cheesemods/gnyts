﻿using AssetsTools.NET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Test.Site
{
    public class BlkManager
    {
        private readonly AssetIndex _assetIndex;
        private readonly string _blkAbPath;
        private readonly Dictionary<int, BlkFile> _blkMapping = new Dictionary<int, BlkFile>();

        public BlkManager(AssetIndex assetIndex, string blkAbPath)
        {
            _assetIndex = assetIndex;
            _blkAbPath = blkAbPath;
        }

        public void GetAssetDetailsById(int assetId, out int blkId, out long offset)
        {
            foreach (var bundleMapping in _assetIndex.blockInfo2)
            {
                foreach (var entry in bundleMapping.entries)
                {
                    if (entry.assetId == assetId)
                    {
                        blkId = bundleMapping.blkId;
                        offset = entry.offset;
                        return;
                    }
                }
            }
            blkId = -1;
            offset = -1;
            return;
        }

        public Mhy0File LoadMhyFile(int assetId)
        {
            int blkId;
            long offset;
            GetAssetDetailsById(assetId, out blkId, out offset);
            return LoadMhyFile(blkId, offset);
        }

        public Mhy0File LoadMhyFile(int blkId, long offset)
        {
            string blkPath = Path.Combine(_blkAbPath, FindBlkFolderName(blkId));
            blkPath = Path.Combine(blkPath, blkId.ToString("d8") + ".blk");
            if (!_blkMapping.ContainsKey(blkId))
            {
                BlkFile blkFile = LoadBlkFile(blkPath);
                _blkMapping[blkId] = blkFile;
            }
            //File.WriteAllBytes(blkId.ToString("d8") + "_" + offset + ".mhy", _blkMapping[blkId].DEBUGdata);
            return _blkMapping[blkId].ReadFile(offset);
        }

        public string FindBlkFolderName(int blkId)
        {
            foreach (var folderMapping in _assetIndex.blockInfo1)
            {
                foreach (var fileMapping in folderMapping.entries)
                {
                    if (fileMapping == blkId)
                    {
                        long folderIdx = folderMapping.chunkId;
                        return folderIdx.ToString("d2");
                    }
                }
            }
            return null;
        }

        public static BlkFile LoadBlkFile(string blkPath)
        {
            return new BlkFile(new AssetsFileReader(File.OpenRead(blkPath)));
        }
    }
}
