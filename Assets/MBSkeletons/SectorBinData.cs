﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MoleMole.Config
{
	[Serializable]
	[CreateAssetMenu(menuName = "ScriptableObjects/SectorBinData")]
	public class SectorBinData : ScriptableObject
	{
		public List<Item> datas;

		[Serializable]
		public struct Item
		{
			public int offset;
			public ulong hash;
		}
	}
}