﻿using UnityEngine;

namespace Assets.MHYAssets
{
    public class MiHoYoGrassGlobalConfigurator : MonoBehaviour
	{
		public float[] m_LOD0DistanceSet;
		public float[] m_LOD1DistanceSet;
		public float[] m_LOD2DistanceSet;
		public float[] m_LOD3DistanceSet;
		public bool[] m_LOD0EnabledSet;
		public bool[] m_LOD1EnabledSet;
		public bool[] m_LOD2EnabledSet;
		public bool[] m_LOD3EnabledSet;
		public int[] m_ViewRange;
		public float m_RandomRotateRangeLow;
		public float m_RandomRotateRangeHigh;
		public float m_RandomScaleRangeLow;
		public float m_RandomScaleRangeHigh;
		public float m_RandomScaleYRangeLow;
		public float m_RandomScaleYRangeHigh;
		public float m_RandomOffsetXRangeLow;
		public float m_RandomOffsetXRangeHigh;
		public float m_RandomOffsetZRangeLow;
		public float m_RandomOffsetZRangeHigh;
		public Material m_LocalWindReproject;
		public Material m_SphereWindMaterial;
		public Mesh m_SphereWindMesh;
		public int m_LocalWindExtent;
	}
}
