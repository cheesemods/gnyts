﻿using UnityEditor;

namespace Test.Site
{
    public class UnityProgressHandler : IProgressHandler
    {
        public void ReportProgress(string text, float progress)
        {
            EditorUtility.DisplayProgressBar("Test.Site", text, progress);
        }

        public void ClearProgress()
        {
            EditorUtility.ClearProgressBar();
        }
    }
}
