﻿using UnityEngine;

namespace Assets.MHYAssets
{
    public class MiHoYoGrassBlock : MonoBehaviour
	{
		public MiHoYoGrassData m_GrassData;
		public float m_HeightScale;
		public int m_HeightMapWidth;
		public int m_HeightMapHeight;
		public int m_HeightMapFormat;
		public float[] m_HeightMapData;
	}
}
